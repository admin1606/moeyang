# -*- coding: utf-8 -*-
"""
Created on Tue Dec  4 14:56:48 2018

@author: Administrator
"""

import pandas as pd
import json
import os
import markdown2
import re
import base64
import pickle as pk
from pathlib import Path

from Optimize_pars.Strategies import techIndicators as ti

# ============================ inputs ============================

with open("tech_plot_settings.json") as f:
    
    input_dict = json.load(f)

symbol_lst = input_dict["symbol_lst"]
start_date = input_dict["date_range"][0]
end_date = input_dict["date_range"][1]

indi_set = {int(k): v for k, v in input_dict["timeframe"].items()}

decay = input_dict["decay"]

func_map = {"HB": ti.HB_glm, "BB%":ti.BBANDS_sp, "w%": ti.WLP_glm, "2k-Rev":ti.TKR_sp, 
            "t/h": ti.TH_sp}

# ================================================================

# ========================== helper ==============================

def time_range_cut(tech_s, start_date, end_date):
    
    return tech_s[(tech_s.index >= pd.to_datetime(start_date)) 
                & (tech_s.index <= pd.to_datetime(end_date))]

# ================================================================
    
    
markdown_html = markdown2.markdown("# Technical Indicators Box Plot")


for symbol_name in symbol_lst:
    
    # write markdown 
    markdown_html += markdown2.markdown("\n## " + symbol_name)
    
    # fetch prices
    price_chart = pk.load(open(str(Path(__file__).parents[1]) + "\\Data\\price_charts\\" \
                               + symbol_name + ".pk", "rb"))
    
    # create series of tech indicators
    tech_s = {}
    graph_df = {}
        
    # main loop
    for time_frame, tech_dict in indi_set.items():
        
        tech_s[time_frame] = {}
        graph_df[time_frame] = {}
    
        # read price_df
        price_df = price_chart.chart_dict[time_frame].set_index("Datetime")
    
        # create series of %roc with the decay
        roc_dict = {}
        
        for d in range(1, decay + 1, 1):
            
            roc_dict["ROC_" + str(d).zfill(2)] = \
                pd.Series(ti.ROC(price_df, d)["ROC_" + str(d)].shift(-d).dropna(), 
                          name = "ROC_" + str(d).zfill(2))
                    
        for tech_name, tech_parm in tech_dict.items():
            
            tech_parm["df"] = price_df
            
            tech_s[time_frame][tech_name] = func_map[tech_name](**{k: v for k, v in tech_parm.items() if k != "target"})
            
            tech_s[time_frame][tech_name] = time_range_cut(tech_s[time_frame][tech_name], start_date, end_date)
            
            graph_df[time_frame][tech_name] = {}

            if "target" in tech_parm.keys():
                
                graph_df[time_frame][tech_name][tech_name + "<=" + str(tech_parm["target"][0])] = {}
                graph_df[time_frame][tech_name][tech_name + ">=" + str(tech_parm["target"][1])] = {}
                
                for k, v in roc_dict.items():
                    
                    v = pd.concat([v,tech_s[time_frame][tech_name]], axis = 1)
    
                    v0 = v[v[tech_name] <= tech_parm["target"][0]]
                    v1 = v[v[tech_name] >= tech_parm["target"][1]]
    
                    graph_df[time_frame][tech_name][tech_name + "<=" + str(tech_parm["target"][0])][k] = v[v[tech_name] <= tech_parm["target"][0]][k]
                    graph_df[time_frame][tech_name][tech_name + ">=" + str(tech_parm["target"][1])][k] = v[v[tech_name] >= tech_parm["target"][1]][k]                
                
                graph_df[time_frame][tech_name][tech_name + "<=" + str(tech_parm["target"][0])] = \
                    pd.concat(graph_df[time_frame][tech_name][tech_name + "<=" + str(tech_parm["target"][0])], axis = 1)
    
                graph_df[time_frame][tech_name][tech_name + ">=" + str(tech_parm["target"][1])] = \
                    pd.concat(graph_df[time_frame][tech_name][tech_name + ">=" + str(tech_parm["target"][1])], axis = 1)                
                                           
            else:
                
                for group in tech_s[time_frame][tech_name].dropna().unique():
                    
                    graph_df[time_frame][tech_name][tech_name + ", " + group] = {}
    
                    for k, v in roc_dict.items():
                        
                        v = pd.concat([v,tech_s[time_frame][tech_name]], axis = 1)
                            
                        graph_df[time_frame][tech_name][tech_name + ", " + group][k] = v[v[tech_name] == group][k]
                        
                    graph_df[time_frame][tech_name][tech_name + ", " + group] = \
                        pd.concat(graph_df[time_frame][tech_name][tech_name + ", " + group], axis = 1)

    # restruct graph df                        
    new_graph_df = {}

    for tf, tech_df in graph_df.items():        

        for tech_name, target_df in tech_df.items():
            
            for target_name, df in target_df.items():

                if tech_name not in new_graph_df.keys():
    
                    new_graph_df[tech_name] = {}
                
                if ">=" in target_name:
                
                    target_tag = tech_name + "_up"
                    
                elif "<=" in target_name:
                    
                    target_tag = tech_name + "_down"
                    
                elif ", " in target_name:
                    
                    target_tag = target_name.replace(", ", "_")
                    
                else:
                    
                    target_tag = target_name
                
                if target_tag not in new_graph_df[tech_name].keys():
                    
                    new_graph_df[tech_name][target_tag] = {}
                    
                new_graph_df[tech_name][target_tag][tf] = df
                        
    for tech_name, target_df in new_graph_df.items():
        
        markdown_html += markdown2.markdown("\n### " + tech_name)
        
        for target_name, tf_df in target_df.items():
            
            add_line = False

            markdown_html += markdown2.markdown("\n#### " + target_name)

            md_tf_title = ""
            md_split = ""
            md_pic = ""
            
#            y0 = min([df.min().min() for df in tf_df.values()]) * 1.05
#            y1 = max([df.max().max() for df in tf_df.values()]) * 1.05
            
            for tf, df in tf_df.items():
                
                pic_file_title = symbol_name + "_" + str(tf) + target_name.replace("<=", " less than ") \
                                 .replace(">=", "higher than ").replace("/", "") + ".png"
                            
                if add_line:
                    
                    md_tf_title += str(tf) + "|"
                    md_split += ":---:|"
                    md_pic += "![" + "timeframe = " + str(tf) + ", " + tech_name \
                                + "](tech_plot_pics\\" + pic_file_title + ")" + "|"
                    
                else:
                    
                    md_tf_title += "|" + str(tf) + "|"
                    md_split += "|:---:" + "|"
                    md_pic += "|![" + "timeframe = " + str(tf) + ", " + tech_name \
                                + "](tech_plot_pics\\" + pic_file_title + ")" + "|"
                    
                    
                add_line = True
                
                # arrange title for each graph
                if "up" in target_name:
                    
                    tech_target = tech_name + ">=" + str(indi_set[tf][tech_name]["target"][1])
                    
                elif "down" in target_name:
                    
                    tech_target = tech_name + "<=" + str(indi_set[tf][tech_name]["target"][0])

                else:
    
                    tech_target = target_name                    
                
                graph_title = symbol_name + ", " + min(df.index).strftime("%Y-%m-%d") + \
                               " ~ " + max(df.index).strftime("%Y-%m-%d") + ", " \
                               "TimeFrame = " + str(tf) + ", " + tech_target
                
                
                plot = df.plot.box(title = graph_title, figsize = (10, 8))
#                plot.set_ylim(y0, y1)
                plot.axhline(y=0, linestyle = "--", color = "r")
                fig = plot.get_figure()
                fig.savefig(os.getcwd() + "\\tech_plot_pics\\" + pic_file_title)
            
            markdown_html += markdown2.markdown("\n" + md_tf_title + "\n" + md_split + "\n" + md_pic, extras = ["tables"])
            
# embeded all picture
pic_str_lst = re.findall(r'<img src=".+\.png"', markdown_html)
pic_file_lst = [pic_str.replace('<img src=', '').replace('"', '') for pic_str in pic_str_lst]
pic_base_lst = []

for pic_file in pic_file_lst:
    
    data_uri = base64.b64encode(open(pic_file, 'rb').read()).decode('utf-8').replace('\n', '')
    pic_base_lst.append("data:image/png;base64,{0}".format(data_uri))
    
for i in range(len(pic_str_lst)):
    
    new_pic_str = pic_str_lst[i].replace(pic_file_lst[i], pic_base_lst[i])
    markdown_html = markdown_html.replace(pic_str_lst[i], new_pic_str)
        
with open("temp_tech_box_plot_report.html", "w+") as f:
    
    f.write(markdown_html)
    

    
    
