# -*- coding: utf-8 -*-
"""
Created on Mon Nov 26 18:10:02 2018

@author: Administrator
"""

from Optimize_pars.Strategies import trade_class as tc, techIndicators as ti

import pandas as pd

# =============================== inputs area =================================
symbol_name = "GBPJPY"
time_frame_hb = 1440
time_frame = 60
spread = 30
hb_prd = 10
hb_target = 0.33
date0 = "2017.10.1"
date1 = "2018.10.1"
graph_title = symbol_name + ", Raw HB"
# =============================================================================

# ============================= helper function ===============================

def adjust_time_index(time_index_lst, start_date, end_date):
    
    time_index_lst = [x for x in time_index_lst if x >= pd.Timestamp(start_date)]
    

def extract_time_index(tech_s, tech_target, delay = 0):
    
    x_start_lst = [x for x in tech_s.index if tech_s.loc[x] >= tech_target \
                           and tech_s.shift(delay + 1).loc[x] < tech_target]
    x_end_lst = [x for x in tech_s.index if tech_s.loc[x] < tech_target \
                           and tech_s.shift(delay + 1).loc[x] >= tech_target]
    
    if delay > 0:
        
        for d in range(1, delay + 1, 1):
            x_start_lst = [x for x in x_start_lst if tech_s.shift(d).loc[x] >= tech_target]
            x_end_lst = [x for x in x_end_lst if tech_s.shift(d).loc[x] >= tech_target]

    return x_start_lst, x_end_lst

# =============================================================================            

# create price chart
price_chart = tc.Price_chart(symbol_name, 30)

# fetch prices
price_chart.fetch_mt4bar(time_frame)
price_chart.fetch_mt4bar(time_frame_hb)

# get tech indicator
hb_df = ti.HB(price_chart.chart_dict[time_frame_hb].set_index("Datetime"), hb_prd).dropna()
hb = hb_df["HB"]
hb_pos = hb_df["HB_pos"]
bb = ti.BBANDS(price_chart.chart_dict[time_frame].set_index("Datetime"), 60, 1).dropna()

# target x to draw start vertical lines
x_start_lst, x_end_lst = extract_time_index(hb, hb_target, delay = 1)
y_start_lst, y_end_lst = extract_time_index(hb_pos, 0.5)

# cut x list into time range
for l in [y_start_lst, y_end_lst, x_start_lst, x_end_lst]:
    adjust_time_index(l, date0, date1)
    
# zip x lst
x_lst = list(zip(x_start_lst, x_end_lst))
y_lst = list(zip(y_start_lst, y_end_lst))

# create panel
panel = tc.Visual_pad(graph_title, time_frame, start_time = date0, end_time = date1)

# draw price
panel.draw_price(price_chart)

# draw lines
panel.draw_tech([bb["BBU"], bb["BBL"]])

# draw shadows
panel.draw_shadows(x_lst, alpha = 0.2)
#panel.draw_shadows(y_lst, color = "blue", alpha = 0.2)
    
# show
panel.show_plot()
hb.loc[hb.index > pd.Timestamp(date0)].plot(title = graph_title)