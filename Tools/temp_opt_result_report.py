# -*- coding: utf-8 -*-
"""
Created on Tue Dec  4 10:34:15 2018

@author: Administrator
"""

import pickle as pk
import pandas as pd


# ====================== helper function ===========================

def opt_dict2df(opt_dict):
    
    for k, v in opt_dict.items():
        
       opt_dict[k] = pd.DataFrame(v)
       
    return pd.concat(opt_dict, axis = 1)


def draw_box_plot(opt_df, symbol, feature):

    df_dict = {}

    for k in opt_df[feature].unique():

        df_dict[k] = opt_df.loc[opt_result[feature] == k, symbol]
        
    pd.concat(df_dict, axis = 1).plot.box(title = symbol + ", " + feature + "v.s. sharp ratio")

# ==================================================================


file_loc = "D:\Google Drive\\work\Moe\BitbucketMoe\Strategy\\raichu_x1_v1_opt_result_at_20181203"

with open(file_loc, "rb") as f:
    
    opt_result = pk.load(f)
    
opt_result = opt_dict2df(opt_result)