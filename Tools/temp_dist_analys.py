# -*- coding: utf-8 -*-
"""
Created on Tue Nov 27 17:36:36 2018

@author: Administrator
"""

import pandas as pd

from Optimize_pars.Strategies import trade_class as tc, techIndicators as ti

# ========================== inputs area ======================================

symbol_lst = ["USDCNH", "XAUUSD", "GBPUSD", "GBPJPY", "EURAUD"]
time_frame = 1440
tech_settings = {"n": 10}
start_date = "2017.10.1"
end_date = "2018.10.1"

# =============================================================================

# get price
pc_dic = {}
for symbol in symbol_lst:    
    pc_dic[symbol] = tc.Price_chart(symbol, spread = 30)
    pc_dic[symbol].fetch_mt4bar(time_frame)
    
# get techs
hb_dic = {}
for symbol in symbol_lst:
    tech_settings["df"] = pc_dic[symbol].chart_dict[time_frame].set_index("Datetime")
    hb_dic[symbol] = ti.HB(**tech_settings)["HB"]
    if start_date != None:
        hb_dic[symbol] = hb_dic[symbol].loc[hb_dic[symbol].index >= pd.Timestamp(start_date)]
    if end_date != None:
        hb_dic[symbol] = hb_dic[symbol].loc[hb_dic[symbol].index <= pd.Timestamp(end_date)]        
    
# visualize
pd.DataFrame(hb_dic).hist(figsize = (12, 8))
pd.DataFrame(hb_dic).plot.box(figsize = (12, 8), title = "Distribution of Raw HB, from " + start_date + " to " + end_date)
