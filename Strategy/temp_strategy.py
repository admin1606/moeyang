# -*- coding: utf-8 -*-
"""
Created on Thu Nov 22 11:48:11 2018

@author: Moe
"""

from Optimize_pars.Strategies import trade_class as tc
import pandas as pd


def main(price_chart, lots, *args, start_date = None, 
         end_date = None):
    
    # any tech indicators required
    pass
    
    # any bar required
    pass

    # initialize the first bar index (if the strategy depending bar)
    bar_index0 = None
    
    # create trade book    
    trade_book = tc.Trade_book(price_chart.name)
    
    # the price df used for loop trade
    price_df = price_chart.chart_dict[0]

    # extract the price df in specific time period 
    if start_date != None:
        
        price_df = price_df.loc[price_df["Datetime"] >= pd.to_datetime(start_date)]
        
    if end_date != None:
        
        price_df = price_df.loc[price_df["Datetime"] <= pd.to_datetime(end_date)]

    # loop the price to trade        
    for index, tick in price_df.iterrows():
        
        # update trade book
        trade_book.update(tick["Datetime"], tick["Ask"], tick["Bid"])
        
        # map the current time to tech indicator's timeframe (if any)
        bar_index1 = tick["Datetime"].floor(str(tech_indicators_time_frame) + "min")
        
        # check if current time adjusted by indicator timeframe exists (if any)
        if bar_index1 != bar_index0 and bar_index1 in tech_indicator_index:
            
            # update bar_index
            bar_index0 = bar_index1
            
            # main trading logic
            pass
        
        # record float status
        trade_book.float_record()
        
    # close all unclosed order at the end of the simulation
    trade_book.close_all()
    
    return trade_book