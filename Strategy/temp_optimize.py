# -*- coding: utf-8 -*-
"""
Created on Thu Nov 22 13:46:45 2018

@author: Administrator
"""

import datetime
import pickle as pk
import multiprocessing as mp

from Optimize_pars import opt_class as oc
from Optimize_pars.Strategies import trade_class as tc

# =================== input area ==============================
    
# input the main function of strategy
from raichu_x2_v1 import main

# inputs required optimize
opt_input = {"bb_prd": 60, "bb_std": 0.5, "bar_time_frame": 30, "sl_pts": 300, 
             "sl_threshold": 500, "sl_step": 100}

# inputs range for brute optimize
opt_rng = {"bb_prd": [x for x in range(30, 400, 30)],
           "bb_std": [x * 0.1 for x in range(5, 20, 5)],
           "bar_time_frame": [30, 60, 240],
           "sl_pts": [x for x in range(100, 2000, 500)],
           "sl_threshold": [x for x in range(100, 1000, 300)],
           "sl_step": [x for x in range(100, 1000, 300)]}


# input bnds for min optimize
#opt_bnd = {}


# inputs fixed
fix_input = {"lots": 0.1, "start_date": "2018.03.01", "end_date": "2018.10.22"}

# create symbol name list
# symbol_lst = ["USDCNH"]
symbol_lst = ["USDCNH", "XAUUSD", "GBPUSD", "GBPJPY", "EURAUD"]

# =============================================================

# ====================== main logic ===========================

# create optimizer
opt = oc.Optimizer(main, opt_input, fix_input)

# create price charts
price_chart_dict = {symbol_name: tc.Price_chart(symbol_name, 30) \
                    for symbol_name in symbol_lst}

# fetch prices
for k, v in price_chart_dict.items():
    v.fetch_all()
    v.trans_tick(60)

# main func
def opt_looper(symbol_name):

    print(symbol_name + " optimize start at " + str(datetime.datetime.now()))

    opt.exhaust_result_pck(price_chart_dict[symbol_name], "all", opt_rng)

    print(symbol_name + " optimize end at " + str(datetime.datetime.now()))

    return (symbol_name, opt.result_dict)


# call multiprocessing
if __name__ == "__main__":

    # optimize by brute, for multiple product
    comb_num = 1
    for i in [len(x) for x in opt_rng.values()]:
        comb_num *= i

    # calculate amount of settings can be run
    print("Amount of setting combinations: " + str(comb_num))

    with mp.Pool(mp.cpu_count() - 1) as p:

        opt_result_lst = p.map(opt_looper, symbol_lst)

    for v in opt_result_lst:
        
        opt.result_dict = {**opt.result_dict, **v}

    #opt.brute_result_pck(**kargs)

    with open("raichu_x2_v1_opt_result_at_" + f"{datetime.datetime.now():%Y%m%d}", "wb") as f:
        pk.dump(opt, f)
