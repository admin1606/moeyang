# -*- coding: utf-8 -*-
"""
Created on Thu Nov 22 16:13:17 2018

@author: Moe
"""

from Optimize_pars.Strategies import trade_class as tc, techIndicators as ti

import pickle as pk
from pathlib import Path

# ============================== input area ===================================

symbol_name = "XAUUSD"
time_frame = 30
bb_prd = 30
bb_std = 0.5
start_date = "2018.3.1"
end_date = "2018.10.22"

# =============================================================================



# input the main function of strategy
from raichu_x3_v1 import main

# input symbol name, fetch price chart 
price_chart = pk.load(open(str(Path(__file__).parents[1]) + "\\Data\\price_charts\\" + symbol_name + ".pk", "rb"))

# input settings
settings = {"price_chart": price_chart, "lots": 0.1, "bar_time_frame": time_frame,
            "bb_prd": bb_prd, "bb_std": bb_std, "start_date": start_date, 
            "end_date": end_date}

# create back-testing trade book
tb = main(**settings)

# visualize back-testing result
panel = tc.Visual_pad(price_chart.name, time_frame, settings["start_date"], 
                      settings["end_date"])
panel.draw_price(price_chart)
panel.draw_trade_book(tb)

# create tech indicator series
bb = ti.BBANDS(price_chart.chart_dict[time_frame].set_index("Datetime"), 
               settings["bb_prd"], settings["bb_std"])

# visualize tech indicators
panel.draw_tech([bb["BBU"], bb["BBL"]])
panel.show_plot()