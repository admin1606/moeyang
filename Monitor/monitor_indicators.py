# -*- coding: utf-8 -*-
"""
Created on Thu Nov 29 16:55:11 2018

@author: Administrator
"""

import pandas as pd
import numpy as np


# ================================ Helper =====================================
# bar builder

def tick2bar(tick, time_frame, direct = "Bid"):
    
    open_price = pd.Series(tick.groupby(pd.Grouper(freq = time_frame))[direct].first(), name = "Open")
    high_price = pd.Series(tick.groupby(pd.Grouper(freq = time_frame))[direct].max(), name = "High")    
    low_price = pd.Series(tick.groupby(pd.Grouper(freq = time_frame))[direct].min(), name = "Low")        
    close_price = pd.Series(tick.groupby(pd.Grouper(freq = time_frame))[direct].last(), name = "Close")

    return pd.concat([open_price, high_price, low_price, close_price], axis = 1)


def concat_bar_by_tick(tick_df, bar_df, bar_df_tf, time_frame):
    
    new_bar = tick2bar(tick_df, bar_df_tf).iloc[-1]
    
    if new_bar.name > bar_df.index[-1]:

        bar_df = bar_df.append(new_bar)

    if time_frame != "1min":
        
        open_price = pd.Series(bar_df.groupby(pd.Grouper(freq = time_frame))["Open"].first())
        high_price = pd.Series(bar_df.groupby(pd.Grouper(freq = time_frame))["High"].max())
        low_price = pd.Series(bar_df.groupby(pd.Grouper(freq = time_frame))["Low"].min())        
        close_price = pd.Series(bar_df.groupby(pd.Grouper(freq = time_frame))["Close"].last())        
        bar_df = (pd.concat([open_price, high_price, low_price, close_price], axis = 1)).dropna()
        
    bar_df["Median"] = (bar_df["High"] + bar_df["Low"])/2
    bar_df["Typical"] = (bar_df["High"] + bar_df["Low"] + bar_df["Close"])/3
    bar_df["Weighted"] = (bar_df["High"] + bar_df["Low"] + 2 * bar_df["Close"])/4
        
    return bar_df


def bar2bar(bar_df, to_tf):
    
    open_price = pd.Series(bar_df.groupby(pd.Grouper(freq = to_tf))["Open"].first(), name = "Open")
    high_price = pd.Series(bar_df.groupby(pd.Grouper(freq = to_tf))["High"].max(), name = "High")    
    low_price = pd.Series(bar_df.groupby(pd.Grouper(freq = to_tf))["Low"].min(), name = "Low")        
    close_price = pd.Series(bar_df.groupby(pd.Grouper(freq = to_tf))["Close"].last(), name = "Close")

    return pd.concat([open_price, high_price, low_price, close_price], axis = 1).dropna()


def concat_bar_by_bar(from_bar_df, to_bar_df, to_bar_tf):
    
    new_bar = bar2bar(from_bar_df, to_bar_tf).iloc[-1]
    
    if new_bar.name > to_bar_df.iloc[-1].name:
        
        to_bar_df = to_bar_df.append(new_bar)
        
    to_bar_df["Median"] = (to_bar_df["High"] + to_bar_df["Low"])/2
    to_bar_df["Typical"] = (to_bar_df["High"] + to_bar_df["Low"] + to_bar_df["Close"])/3
    to_bar_df["Weighted"] = (to_bar_df["High"] + to_bar_df["Low"] + 2 * to_bar_df["Close"])/4        

    return to_bar_df
# =============================================================================

# ========================== tech indicator functions =========================
# number of change 
def NOC(con_df):
    
    if len(con_df) < 2:
        
        return "No enough price available"
    
    return pd.Series(con_df["Close"].iloc[-1] - con_df["Close"].iloc[-2], name = "Amount of Change")


# percentage of chagne
def POC(con_df):

    if len(con_df) < 2:
        
        return "No enough price available"

    return pd.Series((con_df["Close"].iloc[-1] - con_df["Close"].iloc[-2])/ con_df["Close"].iloc[-2], name = "% of Change")


# True Range
def TR(con_df, tr_period):

    local_max = pd.Series(pd.concat([con_df["High"], con_df["Close"].shift()], axis = 1).max(axis = 1) \
                          .rolling(window = tr_period).max(), name = "LocalMax")
    local_min = pd.Series(pd.concat([con_df["Low"], con_df["Close"].shift()], axis = 1).min(axis = 1) \
                          .rolling(window = tr_period).min(), name = "LocalMin")    
        
    TR_s = pd.Series(local_max - local_min, name = "TR")

    return TR_s    


# Average True Range
def ATR(TR_s, ave_period):
    
    ATR_s = pd.Series(TR_s.rolling(window = ave_period, min_periods = ave_period).mean(), name='ATR')

    return ATR_s


# ATR position
def ATRPOS(ATR_s, pos_period):   

    ATR_pos = pd.Series(ATR_s.rolling(window = pos_period).apply(lambda x: pd.Series(x).rank(pct=True).iloc[-1]), name="ATR_pos")
    
    return ATR_pos


# concat version of ATR
def ATR_sp(con_df, ave_period, show_pos = False):
    
    TR_s = TR(con_df, 1)
    ATR_s = ATR(TR_s, ave_period)
    
    if show_pos:
        
        ATR_pos = ATRPOS(ATR_s, 250)
        
        return pd.concat([ATR_s, ATR_pos], axis = 1)
    
    return ATR_s



# HB
def HB(con_df, period, signal):

    con_df.dropna(inplace=True)

    TR_s_n = pd.Series(TR(con_df, period), name = "TR_" + str(period))
    
    TR_s_1 = pd.Series(TR(con_df, 1), name = "TR_" + str(1))    

    ATR_n = pd.Series(ATR(TR_s_1, period), name = "ATR_" + str(period))

    HB = pd.Series(TR_s_n / (period * ATR_n), name="HB")
    
    HB_sig = pd.Series(np.nan, name = "HB_signal", index = HB.index)
    
    HB_sig[HB > signal * 0.01] = "Trend"

    return pd.concat([HB, HB_sig], axis = 1)


# SMA
def SMA(con_df, period, price_type = "Close"):
    
    return pd.Series.rolling(con_df[price_type], period).mean()


# EMA
def EMA(con_df, period, price_type = "Close"):
    
    return pd.Series.ewm(con_df[price_type], span=period, min_periods=period - 1).mean()


# Bollinger Band
def BBANDS(con_df, period, std_k = 2):

    BBM = pd.Series(EMA(con_df, period), name = "BBM")

    MSD = con_df['Close'].rolling(window = period).std()
    
    BBU = pd.Series(BBM + std_k * MSD, name="BBU")
    BBL = pd.Series(BBM - std_k * MSD, name="BBL")

    BB_pos = pd.Series((con_df['Close'] - BBL) / (2 * std_k * MSD), name = "BB%pos")

    return pd.concat([BBM, BBU, BBL, BB_pos], axis = 1)


# William Percentage
def WLP(con_df, period):

    return -(con_df["High"].rolling(window=period).max() - con_df["Close"]) / \
            (con_df["High"].rolling(window=period).max() - con_df["Low"].rolling(window=period).min())


# AO
def AO(con_df, fast_prd, slow_prd):

    con_df["Middle"] = (con_df["High"] + con_df["Low"])/2

    EMAfast = EMA(con_df, fast_prd, price_type = "Middle")

    EMAslow = EMA(con_df, slow_prd, price_type = "Middle")
    
    return pd.Series(EMAfast - EMAslow, name = "AO")


# 2K reverse
def TKR(con_df):

    c2_s =  (con_df["Close"] - con_df["Low"].rolling(window=2).min()) / (con_df['High'].rolling(window=2).max() - con_df['Low'].rolling(window=2).min())
    
    tk_rev = pd.Series(np.nan, index = c2_s.index, name = "2K_Reverse")

    tk_rev[(c2_s.shift(1) > 0.55) & (c2_s < 0.35)] = "DD"

    tk_rev[(c2_s.shift(1) < 0.45) & (c2_s > 0.65)] = 'DU'
    
    return tk_rev


# 3 Day Trend
def TDT(con_df):
    
    c3_s = (con_df["Close"] - con_df["Low"].rolling(window=3).min()) / \
           (con_df['High'].rolling(window=3).max() - con_df['Low'].rolling(window=3).min())
           
    tdt_s = pd.Series(np.nan, index = c3_s.index, name = "3Day_Reverse")
    
    tdt_s[c3_s > 0.6] = "Buy"
    tdt_s[c3_s < 0.4] = "Sell"
    
    tdt_s.fillna(method = "ffill", inplace = True)
    
    return tdt_s


# Tower Hanger
def TH(con_df):
    
    us_s = (con_df[["Open", "Close"]].max(axis=1) - con_df["High"]) / (con_df["High"] - con_df["Low"])

    ls_s = (con_df[["Open", "Close"]].min(axis=1) - con_df["Low"]) / (con_df["High"] - con_df["Low"])
    
    th_s = pd.Series(np.nan, index = us_s.index)
    
    th_s[ls_s > 0.5] = "HANG"
    th_s[us_s < -0.5] = "TOWER"
    
    return th_s