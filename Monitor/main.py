# -*- coding: utf-8 -*-
"""
Created on Fri Nov 30 11:59:44 2018

@author: Administrator
"""

import pandas as pd
import monitor_indicators as mi
import psycopg2
import json

DATABASES = {
    'DBNAME': "bfjfunds",
    'DBUSER': "bfjfunds",
    'DBPASSWORD': "BFJFunds2017",
    'DBHOST': "bfjfunds.chkx8ynoq5pn.ap-southeast-2.rds.amazonaws.com",
    'DBPORT': "5432"
}

def convert_query_result_dict(fields, data):
    """
    convert original python sql query result to dictonary
    :param fields_name:
    :param result:
    :return:
    """
    result = []
    for item in data:
        result.append(dict(zip(fields, item)))

    return result

def connect_db():
    """
    Database connection
    """
    try:
        conn = psycopg2.connect(dbname=DATABASES['DBNAME'], user=DATABASES['DBUSER'], password=DATABASES['DBPASSWORD'], host=DATABASES['DBHOST'], port=DATABASES['DBPORT'])
        print("connecting")
    except:
        print("Error on connecting to PostgreSQL")

    return conn

    
def main(tick_df, bar_df_dict, parm_dict, symbol):
    
    func_dict = {"change": mi.NOC, 
                 "change_per": mi.POC, 
                 "atr": mi.ATR_sp, 
                 "hb": mi.HB, 
                 "sma": mi.SMA, 
                 "ema": mi.EMA, 
                 "bb": mi.BBANDS, 
                 "w_per": mi.WLP, 
                 "ao": mi.AO,
                 "2kr": mi.TKR, 
                 "3dt": mi.TDT, 
                 "th": mi.TH}
    
    output_dict = {}
    con_df_dict = {"M30": mi.concat_bar_by_tick(tick_df, bar_df_dict["1m"], "1min", "30min")}

    # maping dfs to corresponding timeframe name
    tf_dict = {"1h": "H1","4h": "H4", "1d": "D1", "1w": "W1", "1mn": "Mn1"}

    for tf_key, tf_name in tf_dict.items():

        con_df_dict[tf_name] = bar_df_dict[tf_key]

    # concreate latest bar into timeframes
    tf_name_lst = list(tf_dict.values())
    tf_name_lst.insert(0, "M30")
    tf_key_lst = ["30min", "1h", "4h", "1d", "1w", "1MS"]
        
    for index in range(1, len(tf_name_lst)):

        con_df_dict[tf_name_lst[index]] = mi.concat_bar_by_bar(con_df_dict[tf_name_lst[index - 1]], con_df_dict[tf_name_lst[index]], 
                                                             tf_key_lst[index])
        
                
    for tf, ind in parm_dict[symbol].items():
        
        con_df = con_df_dict[tf]
        output_dict[tf] = {}
        
        for k, v in ind.items():
        
            func = func_dict[v["indicator"]]

            settings = v.copy()
            
            if "time_frame" in v.keys():
                
                settings["con_df"] = con_df_dict[v["time_frame"]]
                del settings["time_frame"]
                
            else:

                settings["con_df"] = con_df

            del settings["indicator"]
            
            result = func(**settings)
            
            if type(result) == pd.core.frame.DataFrame:
            
                result = result.iloc[-1]
                result[pd.isnull(result)] = result[pd.isnull(result)].apply(str)                
                result = dict(result)
            
            if type(result) == pd.core.series.Series:
                
                result = result.iloc[-1]
                if pd.isnull(result):
                    result = str(result)
                
            output_dict[tf][k] = result
                
    return output_dict

# ================================== TEST =====================================

with open("sample_input_params_v2.json") as f:
    parm_dict = json.loads(f.read().replace("'", '"'))

import price_format as pf

# initial db cursor
cur = connect_db().cursor()

# initial timeframe list
tf_lst = ["1m", "1h", "4h", "1d", "1w", "1mn"]

# initial symbol name
symbol_name = "GBPUSD"

# initial bar_df_lst
bar_df_dict = {}

# fetch ohlc prices
for tf in tf_lst:

    # prepare sql
    sql_ohlc = '''
        select record_date, record_time, open, high, low, close
        from bfj_price_ohlc_''' + tf + "_temp"\
        '''
        where symbol = %s and broker_id = 1'''
    
    # execute sql query
    cur.execute(sql_ohlc, [symbol_name])
    fields_ohlc = [field[0] for field in cur.description]
    data = cur.fetchall()
    
    # convert result to df
    bar_df_dict[tf] = pf.ohlc_bar_serv(data)

# prepare sql for tick prices    
sql_tick = '''
        select record_date, record_time, ask, bid
        from bfj_price_tick_temp
        where symbol = %s and broker_id = 1'''

# execute sql query
cur.execute(sql_tick, [symbol_name])
fields_ohlc = [field[0] for field in cur.description]
data = cur.fetchall()

# convert result to df
tick_df = pf.tick_serv(data)

# run
output = main(tick_df, bar_df_dict, parm_dict, symbol_name)