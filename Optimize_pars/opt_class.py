# -*- coding: utf-8 -*-
"""
Created on Mon Nov 19 16:06:35 2018

@author: Moe Yang
"""

from itertools import product

import pandas as pd


class Optimizer:
    """ Optimize inputs for certain strategy.

    Attributes:
        strategy_main: The main function of strategy's trading logic.
        fix_dict: The dict of fix inputs which do not need optimize.
        result_dict: The dict to record the result of optimization.
    """

    def __init__(self, strategy_main, fix_inputs):
        """ Initialize optimizer to pass strategy main function and
        fix inputs.

        Args:
            See base class.
        """

        self.strategy_main = strategy_main
        self.fix_dict = fix_inputs
        self.result_dict = {}

    def opt_main_ex(self, opt_dict, symbol_price_chart):
        """ Pass inputs, run trading logic and record the main features
        of results.

        Args:
            opt_dict: dict of inputs mean to optimize.
            symbol_price_chart: Price_chart class of certain symbol.

        Return:
            A dict with features of the trade result.
        """

        self.fix_dict["price_chart"] = symbol_price_chart

        tb = self.strategy_main(**{**opt_dict, **self.fix_dict})

        all_feat = {"pnl": tb.get_total_pnl(), "sharp ratio": tb.get_sharp_ratio(),
                    "win rate": tb.get_win_rate(), "P/L": tb.get_pl_ratio(),
                    "no. of trade": tb.get_trade_len()}

        return all_feat

    def exhaust_result_pck(self, price_chart, opt_range):
        """ Generate every combination of inputs and run the trading logic.
        Record the result in self.result_dict.

        Args:
            price_chart: Price_chart class of certain symbol.
            opt_range: A dict of list of all want-to-try value for the inputs mean to optimize.
        """

        # Arrange all combination for all possible value of want-to-optimize inputs, generate a list
        # of dict which can be feed in trading function directly.
        set_lst = [dict(zip(opt_range, v)) for v in product(*opt_range.values())]

        # Record inputs in self.result_dict.
        self.result_dict["settings"] = {}
        for k in set_lst[0].keys():
            self.result_dict["settings"][k] = {}

        # Loop the combination of inputs.
        result = {}
        for set_pck in set_lst:
            result[str(set_pck)] = self.opt_main_ex(set_pck, price_chart)
            for k, v in set_pck.items():
                self.result_dict["settings"][k][str(set_pck)] = v

        # Record the trading result of each set of inputs.
        self.result_dict[price_chart.name] = result

    def get_opt_result(self):
        """ Reformat result_dict into dataframe.

        Return:
            A dataframe of optimize result.
        """

        df_dict = {}
        for k, v in self.result_dict.items():
            if k == "settings":
                df_dict[k] = pd.DataFrame(v)
            else:
                df_dict[k] = pd.DataFrame(v).T

        return pd.concat(df_dict, axis=1, sort=True)