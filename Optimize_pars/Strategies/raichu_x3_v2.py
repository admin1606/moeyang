# -*- coding: utf-8 -*-
"""
Created on Mon Nov 19 10:55:08 2018

@author: Administrator
"""

# =============================================================================
# Raichu filterred by HB
# HB setting is hard code (timeframe = 1440, period = 10, trigger value = 0.33)
# =============================================================================

import pandas as pd
import bisect

from Optimize_pars.Strategies import trade_class as tc, techIndicators as ti


def main(price_chart, lots, bar_time_frame, bb_prd, bb_std, hb_time_frame = 1440,
         hb_prd = 10, hb_trg = 0.33, start_date = None, end_date = None):
    
    if bar_time_frame not in price_chart.chart_dict.keys():

        tf_lst = list(price_chart.chart_dict.keys())
        tf_lst.sort()
        from_tf_index = bisect.bisect(tf_lst, bar_time_frame)
        price_chart.trans_bar(tf_lst[from_tf_index], int(bar_time_frame))
    
    bb = ti.BBANDS_sp(price_chart.chart_dict[int(bar_time_frame)].set_index("Datetime"), 
                      bb_prd, bb_std).dropna().shift()
    hb = ti.HB_glm(price_chart.chart_dict[hb_time_frame].set_index("Datetime"), hb_prd).dropna().shift()
    trade_book = tc.Trade_book(price_chart.name)
    bar_index0 = None
    price_df = price_chart.chart_dict[0]

    if start_date != None:
        
        price_df = price_df.loc[price_df["Datetime"] >= pd.to_datetime(start_date)]
        
    if end_date != None:
        
        price_df = price_df.loc[price_df["Datetime"] <= pd.to_datetime(end_date)]        
    
    for index, tick in price_df.iterrows():
        
        # update trade book
        trade_book.update(tick["Datetime"], tick["Ask"], tick["Bid"])
        
        # check if valid bb / next new bar
        bar_index1 = tick["Datetime"].floor(str(bar_time_frame) + "min")
        
        # check if valid hb
        hb_index = tick["Datetime"].floor(str(hb_time_frame) + "min")
        
        
        if bar_index1 != bar_index0 and bar_index1 in bb.index \
            and tick["Datetime"].floor("1440min") in hb.index:
            
            # update bar_index
            bar_index0 = bar_index1
            
            # check if exists live order
            if trade_book.live_orders != {}:
                
                if hb.loc[hb_index] < 0.33:
                    
                    trade_book.close_all()
                    
                else:
                
                    if list(trade_book.live_orders.values())[0]["type"] == "buy":
                        
                        if bb.loc[bar_index1] < 0:
                            
                            trade_book.close_all()
                            
                    elif list(trade_book.live_orders.values())[0]["type"] == "sell":
                        
                        if bb.loc[bar_index1] > 1:
                            
                            trade_book.close_all()
            
            if trade_book.live_orders == {}:
                
                if hb.loc[hb_index] > 0.33 and hb.shift().loc[hb_index] > 0.33:
                    
                    if bb.loc[bar_index1] > 1 and bb.shift().loc[bar_index1] < 1:
                    
                        trade_book.send_market_order(lots, "buy")
                        
                    elif bb.loc[bar_index1] < 0 and bb.shift().loc[bar_index1] > 0:
                        
                        trade_book.send_market_order(lots, "sell")
      
        trade_book.float_record()
        
    trade_book.close_all()
    
    return trade_book