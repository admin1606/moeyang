# -*- coding: utf-8 -*-
"""
Created on Thu Nov 15 11:01:39 2018

@author: Moe Yang
"""

# ============================================================================= 
# Basic Version of Low Frequency Scalping
# Auto-adjusted step & TP by ATR
# ============================================================================= 


from Optimize_pars.Strategies import trade_class as tc, techIndicators as ti
import pandas as pd
        


def main(price_chart, lots, top_lv, bot_lv, bar_time_frame, atr_time_frame, atr_period, 
         atr_step_ratio, atr_tp_ratio, time_step, start_date, end_date):
    
    '''
    1. Price_chart price_chart
    2. float lots
    3. int top_lv
    4. int bot_lv
    5. int time_frame (by minutes)
    6. int atr_time_frame (by minutes)
    7. int atr_period
    8. float atr_step_ratio
    9. float atr_tp_ratio
    10. str time_step (e.g. "2mins")
    '''
    
    atr = ti.ATR_sp(price_chart.chart_dict[atr_time_frame].set_index("Datetime"), atr_period).shift().dropna()
    trade_book = tc.Trade_book(price_chart.name)
    
    # trans all tf from ticks
    for tf in set([bar_time_frame, atr_time_frame]):
        price_chart.drop_charts(tf)
        price_chart.trans_bar(0, tf)

    price_df = price_chart.chart_dict[0]
    
    # extract the price df in specific time period 
    if start_date != None:
        
        price_df = price_df.loc[price_df["Datetime"] >= pd.to_datetime(start_date)]
        
    if end_date != None:
        
        price_df = price_df.loc[price_df["Datetime"] <= pd.to_datetime(end_date)]    
    
    first_run = True

    for index, tick in price_df.iterrows():

        # update trade book
        trade_book.update(tick["Datetime"], tick["Ask"], tick["Bid"])
                
        # check if the first time run
        if first_run:

            # check if valid atr signal
            if tick["Datetime"].floor(str(atr_time_frame) + "min") not in atr.index:
                
                continue
                
            atr_step = atr_step_ratio * atr.loc[tick["Datetime"].floor(str(atr_time_frame) + "min")]
            tp_step = atr_tp_ratio * atr.loc[tick["Datetime"].floor(str(atr_time_frame) + "min")]
            
            for n in range(top_lv):
                
                open_price = tick["Ask"] + atr_step * (n + 1)
                tp_price = open_price - tp_step
                trade_book.send_pend_order(lots, "sell limit", open_price, tp_price = tp_price)
                
            for m in range(bot_lv):
                
                open_price = tick["Bid"] - atr_step * (n + 1)
                tp_price = open_price + tp_step
                trade_book.send_pend_order(lots, "buy limit", open_price, tp_price = tp_price)
                
            # update time point
            time_0 = tick["Datetime"]
            first_run  = False
                
        elif tick["Datetime"] - time_0 >= pd.Timedelta(time_step):
            
            # cancel untriggered pending orders
            trade_book.pend_orders.clear()
            
            # update time_0
            time_0 = tick["Datetime"]

            # update step
            if tick["Datetime"].floor(str(atr_time_frame) + "min") in atr.index:            
                atr_step = atr_step_ratio * atr.loc[tick["Datetime"].floor(str(atr_time_frame) + "min")]
                tp_step = atr_tp_ratio * atr.loc[tick["Datetime"].floor(str(atr_time_frame) + "min")]
            
            for n in range(top_lv):
                
                open_price = tick["Ask"] + atr_step * (n + 1)
                tp_price = open_price - tp_step
                trade_book.send_pend_order(lots, "sell limit", open_price, tp_price = tp_price)
                
            for m in range(bot_lv):
                
                open_price = tick["Bid"] - atr_step * (n + 1)
                tp_price = open_price + tp_step
                trade_book.send_pend_order(lots, "buy limit", open_price, tp_price = tp_price)
                
    trade_book.close_all("close by stop")
                                
    return trade_book            
            
        
        
        