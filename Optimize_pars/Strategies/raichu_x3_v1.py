# -*- coding: utf-8 -*-
"""
Created on Mon Nov 19 10:55:08 2018

@author: Administrator
"""

# =============================================================================
# Basic version of Raichu
# Open three orders if break BB bonds 
# 
# =============================================================================

import pandas as pd

from Optimize_pars.Strategies import trade_class as tc, techIndicators as ti


def main(price_chart, lots, bar_time_frame, bb_prd, bb_std, start_date = None,
         end_date = None):
    
    bb = ti.BBANDS_sp(price_chart.chart_dict[int(bar_time_frame)] \
           .set_index("Datetime"), bb_prd, bb_std).dropna().shift()

    trade_book = tc.Trade_book(price_chart.name)

    bar_index0 = None

    price_df = price_chart.trans_tick(bar_time_frame)

    if start_date != None:
        
        price_df = price_df.loc[price_df["Datetime"] >= pd.to_datetime(start_date)]
        
    if end_date != None:
        
        price_df = price_df.loc[price_df["Datetime"] <= pd.to_datetime(end_date)]        
    
    for index, tick in price_df.iterrows():
        
        # update trade book
        trade_book.update(tick["Datetime"], tick["Ask"], tick["Bid"])
        
        # check if valid bb / next new bar
        bar_index1 = tick["Datetime"].floor(str(bar_time_frame) + "min")
        
        if bar_index1 != bar_index0 and bar_index1 in bb.index:
            
            # update bar_index
            bar_index0 = bar_index1
            
            # check if exists live order
            if trade_book.live_orders != {}:
                
                if list(trade_book.live_orders.values())[0]["type"] == "buy":
                    
                    if bb.loc[bar_index1] < 0:
                        
                        trade_book.close_all()
                        
                elif list(trade_book.live_orders.values())[0]["type"] == "sell":
                    
                    if bb.loc[bar_index1] > 1:
                        
                        trade_book.close_all()
            
            if trade_book.live_orders == {}:

                if bb.loc[bar_index1] > 1 and bb.shift().loc[bar_index1] < 1:
                
                    trade_book.send_market_order(lots, "buy")
                    
                elif bb.loc[bar_index1] < 0 and bb.shift().loc[bar_index1] > 0:
                    
                    trade_book.send_market_order(lots, "sell") 
        
        trade_book.float_record()
        
    trade_book.close_all()
    
    return trade_book