# -*- coding: utf-8 -*-
"""
Created on Thu Nov 22 11:48:11 2018

@author: Moe
"""

from Optimize_pars.Strategies import trade_class as tc, techIndicators as ti

import pandas as pd


def main(price_chart, lots, bar_time_frame, bb_prd, bb_std, sl_pts, sl_threshold, 
         sl_step, start_date = None, end_date = None):
    
    # any tech indicators required
    bb = ti.BBANDS_sp(price_chart.chart_dict[bar_time_frame].set_index("Datetime"), bb_prd, bb_std).shift()

    # initialize the first bar index (if the strategy depending bar)
    bar_index0 = None
    
    # create trade book    
    trade_book = tc.Trade_book(price_chart.name)
    
    # the price df used for loop trade
    price_df = price_chart.trans_tick(bar_time_frame)

    # extract the price df in specific time period 
    if start_date != None:
        
        price_df = price_df.loc[price_df["Datetime"] >= pd.to_datetime(start_date)]
        
    if end_date != None:
        
        price_df = price_df.loc[price_df["Datetime"] <= pd.to_datetime(end_date)]
        
    # initialize trade_locks
    buy_trade_lock = True
    sell_trade_lock = True
    
    # initialize pts to the real prices
    sl_threshold = sl_threshold/price_chart.size
    sl_step = sl_step/price_chart.size
    sl_pts = sl_pts/price_chart.size
    
    # loop the price to trade        
    for index, tick in price_df.iterrows():
        
        # update trade book
        trade_book.update(tick["Datetime"], tick["Ask"], tick["Bid"])

        # update trade locks
        if trade_book.live_orders != {}:
            
            if list(trade_book.live_orders.values())[0]["type"] == "buy":
                
                buy_trade_lock = False
                sell_trade_lock = True
                
            elif list(trade_book.live_orders.values())[0]["type"] == "sell":
                
                sell_trade_lock = False
                buy_trade_lock = True
        
        # map the current time to tech indicator's timeframe (if any)
        bar_index1 = tick["Datetime"].floor(str(bar_time_frame) + "min")
        
        # check if current time adjusted by indicator timeframe exists (if any)
        if bar_index1 != bar_index0 and bar_index1 in bb.index:
            
            # update bar_index
            bar_index0 = bar_index1
                        
            # check if exists live order
            if trade_book.live_orders == {}:
                
                # send market order by BB bands                
                if buy_trade_lock == True and bb.loc[bar_index1] > 1 and bb.shift().loc[bar_index1] <= 1:
                    trade_book.send_market_order(lots, "buy", sl_price = tick["Ask"] - sl_pts)
                
                if sell_trade_lock == True and bb.loc[bar_index1] < 0 and bb.shift().loc[bar_index1] >= 0:
                    trade_book.send_market_order(lots, "sell", sl_price = tick["Bid"] + sl_pts)
                    
            else:
                # modify sl of live orders                
                for k, v in trade_book.live_orders.items():
                    
                    if v["Note"] == "modified":
                                           
                        if v["type"] == "buy":
                                                
                            if tick["Bid"] - v["SL"] > sl_step:
                                
                                v["SL"] += sl_step
                            
                        elif v["type"] == "sell":
                                                
                            if v["open price"] - tick["Ask"] > sl_step:
                                
                                v["SL"] -= sl_step
                                
                    else:
                        
                        if v["type"] == "buy":
                            
                            if tick["Bid"] - v["open price"] > sl_threshold:
                                
                                v["SL"] = v["open price"]
                                v["Note"] = "modified"
                                
                        elif v["type"] == "sell":
                            
                            if v["open price"] - tick["Ask"] > sl_threshold:
                                
                                v["SL"] = v["open price"]
                                v["Note"] = "modified"
        
        # record float status
        trade_book.float_record()
        
    # close all unclosed order at the end of the simulation
    trade_book.close_all()
    
    return trade_book