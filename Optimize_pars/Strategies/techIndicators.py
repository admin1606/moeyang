# retrieved from https://www.quantopian.com/posts/technical-analysis-indicators-without-talib-code
# @author: Bruno Franca
# @author: Peter Bakker
# date: May 6 2017
# update: Jan 16 2018 by Moe

import pandas as pd
import numpy as np


# helper
def cum_counter(y):
    return y * (y.groupby((y != y.shift()).cumsum()).cumcount() + 1)


# ===================================

# Moving Average
def MA(df, n):
    # Original version
    # MA = pd.Series(pd.rolling_mean(df['Close'], n), name = 'MA_' + str(n))
    MA = pd.Series.rolling(df['Close'], n).mean()
    MA = pd.Series(MA, name='MA')
    MA_div = pd.Series(df['Close'] / MA - 1, name='MA%div')
    df = df.join(MA).join(MA_div)
    return df

def MA_sp(df, n):
    MA = pd.Series.rolling(df['Close'], n).mean()
    return MA


# Exponential Moving Average
def EMA(df, n):
    # Orignial version
    # EMA = pd.Series(pd.ewma(df['Close'], span = n, min_periods = n - 1), name = 'EMA_' + str(n))
    EMA = pd.Series.ewm(df['Close'], span=n, min_periods=n).mean()
    EMA = pd.Series(EMA, name='EMA')
    EMA_div = pd.Series(df['Close'] / EMA - 1, name='EMA%div')
    df = df.join(EMA).join(EMA_div)
    return df

def EMA_sp(df, n):
    EMA = pd.Series.ewm(df['Close'], span=n, min_periods=n).mean()
    return EMA


# MA for glm
def MA_glm(df, n_fast, n_slow, EMA_method = False, ratio = True):
    if EMA_method:
        MA_fast = pd.Series.ewm(df['Close'], span=n_fast, min_periods=n_fast).mean()
        MA_slow = pd.Series.ewm(df['Close'], span=n_slow, min_periods=n_slow).mean()
    else:
        MA_fast = pd.Series.rolling(df['Close'], n_fast).mean()
        MA_slow = pd.Series.rolling(df['Close'], n_slow).mean()
    diff = pd.Series(MA_fast - MA_slow, name = 'MA_diff')
    if ratio:
        ratio = pd.Series(diff/MA_slow, name = 'MA%')
        return ratio
    else:
        return diff
    
        
# Momentum
def MOM(df, n):
    M = pd.Series(df['Close'].diff(n), name='Momentum_' + str(n))
    df = df.join(M)
    return df


# Rate of Change
def ROC(df, n):
    M = df['Close'].diff(n)
    N = df['Close'].shift(n)
    ROC = pd.Series(M / N, name='ROC_' + str(n))
    df = df.join(ROC)
    return df

# Rate of Change on Pivot Point
def ROC_PP(df, n):
    df = PPSR(df)
    M = df['PP'].diff(n)
    N = df['PP'].shift(n)
    ROC = pd.Series(M / N, name='ROC_' + str(n))
    df = df.join(ROC)
    return df

# Average True Range
# update: tr_n = period for true range, ave_n = period for average period
def ATR(df, tr_n, ave_n, win = 250):
    local_max = pd.Series(pd.concat([df["High"], df["Close"].shift()], axis = 1).max(axis = 1) \
                          .rolling(window = tr_n).max(), name = "LocalMax")
    local_min = pd.Series(pd.concat([df["Low"], df["Close"].shift()], axis = 1).min(axis = 1) \
                          .rolling(window = tr_n).min(), name = "LocalMin")    
    TR_s = pd.Series(local_max - local_min, name = "TR")
    ATR = pd.Series(TR_s.rolling(window = ave_n, min_periods = ave_n).mean(), name='ATR')
    ATR_pos = pd.Series(ATR.rolling(window=win).apply(lambda x: pd.Series(x).rank(pct=True).iloc[-1]), name="ATR_pos")
    df = df.join(TR_s).join(ATR).join(ATR_pos)
    return df

# True Range ONLY faster version
def TR_sp(df, tr_n):
    local_max = pd.Series(pd.concat([df["High"], df["Close"].shift()], axis = 1).max(axis = 1) \
                          .rolling(window = tr_n).max(), name = "LocalMax")
    local_min = pd.Series(pd.concat([df["Low"], df["Close"].shift()], axis = 1).min(axis = 1) \
                          .rolling(window = tr_n).min(), name = "LocalMin")    
    TR_s = pd.Series(local_max - local_min, name = "TR")
    return TR_s


# Average True Range ONLY faster version
def ATR_sp(df, ave_n):
    local_max = pd.Series(pd.concat([df["High"], df["Close"].shift()], axis = 1).max(axis = 1), name = "LocalMax")
    local_min = pd.Series(pd.concat([df["Low"], df["Close"].shift()], axis = 1).min(axis = 1), name = "LocalMin")    
    TR_s = pd.Series(local_max - local_min, name = "TR")
    ATR_s = pd.Series(TR_s.rolling(window = ave_n, min_periods = ave_n).mean(), name='ATR')
    return ATR_s


def ATR_glm(df, n):
    local_max = pd.Series(pd.concat([df["High"], df["Close"].shift()], axis = 1).max(axis = 1), name = "LocalMax")
    local_min = pd.Series(pd.concat([df["Low"], df["Close"].shift()], axis = 1).min(axis = 1), name = "LocalMin")    
    TR = pd.Series(local_max - local_min, name = "TR")
    ATR = pd.Series(TR.ewm(alpha=1 / n, min_periods=n).mean(), name='ATR')    
    return ATR





# Bollinger Bands (update)
def BBANDS(df, n, k=2, EMA_method=False):
    n = int(n)
    if EMA_method == True:
        MA = df['Close'].ewm(span=n, min_periods=n).mean()
    else:
        MA = df['Close'].rolling(window=n).mean()
    MSD = df['Close'].rolling(window=n).std()
    BU = pd.Series(MA + k * MSD, name="BBU")
    BL = pd.Series(MA - k * MSD, name="BBL")
    b2 = (df['Close'] - BL) / (2 * k * MSD)
    temp_df = pd.DataFrame({'BB%': b2})
    temp_df.loc[(temp_df['BB%'] > 0.75) & (temp_df['BB%'] < 1), "Boll_pos"] = 4
    temp_df.loc[(temp_df['BB%'] < 0.25) & (temp_df['BB%'] > 0), "Boll_pos"] = 1
    temp_df.loc[temp_df['BB%'] > 1, "Boll_pos"] = "P"
    temp_df.loc[temp_df['BB%'] < 0, "Boll_pos"] = "G"
    df = df.join(BU).join(BL).join(temp_df)
    return df

# Bollinger Bands (update2)
def BBANDS_2(df, n, k=2, EMA_method=False):
    n = int(n)
    if EMA_method == True:
        MA = df['Close'].ewm(span=n, min_periods=n).mean()
    else:
        MA = df['Close'].rolling(window=n).mean()

    MSD = df['Close'].rolling(window=n).std()
    BU = pd.Series(MA + k * MSD, name="BBU")
    BL = pd.Series(MA - k * MSD, name="BBL")
    bo = (df['Open'] - BL) / (2 * k * MSD)
    bh = (df['High'] - BL) / (2 * k * MSD)
    bl = (df['Low'] - BL) / (2 * k * MSD)
    temp_df = pd.DataFrame({'B%O': bo, 'B%H': bh, 'B%L': bl})
    temp_df = temp_df.join(BU).join(BL)
    return temp_df

# Bollinger Bands faster version
def BBANDS_sp(df, n, k=2, EMA_method=False):
    n = int(n)
    if EMA_method == True:
        MA = df['Close'].ewm(span=n, min_periods=n).mean()
    else:
        MA = df['Close'].rolling(window=n).mean()
    MSD = df['Close'].rolling(window=n).std()
    BL = pd.Series(MA - k * MSD, name="BBL")
    b2 = (df['Close'] - BL) / (2 * k * MSD)
    temp_df = pd.Series(b2, name = "BB%")
    return temp_df

def BBANDS_glm(df, n, k=2, EMA_method=False):
    if EMA_method == True:
        MA = df['Close'].ewm(span=n, min_periods=n).mean()
    else:
        MA = df['Close'].rolling(window=n).mean()
    MSD = df['Close'].rolling(window=n).std()
    BL = pd.Series(MA - k * MSD)
    b2 = pd.Series((df['Close'] - BL) / (2 * k * MSD), name = 'BB%_' + str(k) + 'var')
    return b2


# Pivot Points, Supports and Resistances
def PPSR(df):
    PP = pd.Series((df['High'] + df['Low'] + df['Close']) / 3)
    R1 = pd.Series(2 * PP - df['Low'])
    S1 = pd.Series(2 * PP - df['High'])
    R2 = pd.Series(PP + df['High'] - df['Low'])
    S2 = pd.Series(PP - df['High'] + df['Low'])
    R3 = pd.Series(df['High'] + 2 * (PP - df['Low']))
    S3 = pd.Series(df['Low'] - 2 * (df['High'] - PP))
    psr = {'PP': PP, 'R1': R1, 'S1': S1, 'R2': R2, 'S2': S2, 'R3': R3, 'S3': S3}
    PSR = pd.DataFrame(psr)
    df = df.join(PSR)
    return df


# Stochastic oscillator %K
def STOK(df, n):
    low = df['Low'].rolling(n).min()
    high = df['High'].rolling(n).max()
    SOk = pd.Series((df['Close'] - low) / (high - low), name='SO%k')
    df = df.join(SOk)
    return df

# Stochastic oscillator %D
def STO(df, n, sig_n = 3, EMA_method = False):
    low = df['Low'].rolling(n).min()
    high = df['High'].rolling(n).max()
    SOk = pd.Series((df['Close'] - low) / (high - low), name='SO%k')
    if EMA_method:
        SOd = pd.Series(SOk.ewm(span=sig_n, min_periods=sig_n - 1).mean(), name='SO%d_' + str(n))
    else:
        SOd = pd.Series(SOk.rolling(sig_n).mean(), name = 'SO%d_' + str(n))
    df = df.join(SOd)
    return df

def STO_glm(df, n, sig_n = 3, EMA_method = False):
    low = df['Low'].rolling(n).min()
    high = df['High'].rolling(n).max()
    SOk = (df['Close'] - low) / (high - low)
    if EMA_method:
        SOd = pd.Series(SOk.ewm(span=sig_n, min_periods=sig_n - 1).mean(), name = 'SO_D%')
    else:
        SOd = pd.Series(SOk.rolling(sig_n).mean(), name = 'SO_D%')
    return SOd


# Trix
def TRIX(df, n):
    EX1 = df['Close'].ewm(span=n, min_periods=n).mean()
    EX2 = EX1.ewm(span=n, min_periods=n).mean()
    EX3 = EX2.ewm(span=n, min_periods=n).mean()
    ROC_l = EX3.diff(1) / EX3.shift(1)
    Trix = pd.Series(ROC_l, name='Trix_' + str(n))
    df = df.join(Trix)
    return df


# Average Directional Movement Index
def ADX(df, n, n_ADX):
    temp_df = pd.DataFrame({"HL": df["High"] - df["Low"],
                            "HC": abs(df["High"] - df["Close"].shift(1)),
                            "CL": abs(df["Low"] - df["Close"].shift(1))})
    TR_l = (temp_df[["HL", "HC", "CL"]].max(axis=1).ewm(alpha=1 / n).mean()) * n
    pos_M = df["High"] - df["High"].shift(1)
    neg_M = df["Low"].shift(1) - df["Low"]
    DX_df = pd.DataFrame({"pos_M": pos_M,
                          "neg_M": neg_M,
                          "DM_up": pos_M[(pos_M > neg_M) & (pos_M > 0)],
                          "DM_down": neg_M[(neg_M > pos_M) & (neg_M > 0)]})
    DX_df[["DM_up", "DM_down"]] = DX_df[["DM_up", "DM_down"]].fillna(0).ewm(alpha=1 / n).mean()
    DI_up = DX_df["DM_up"] / TR_l
    DI_down = DX_df["DM_down"] / TR_l
    ADX = pd.Series(pd.Series.ewm(abs(DI_up - DI_down) / (DI_up + DI_down), alpha=1 / n_ADX, min_periods=n_ADX).mean(),
                    name='ADX_' + str(n) + '_' + str(n_ADX))
    df = df.join(pd.Series(DI_up, name="DI+")).join(pd.Series(DI_down, name="DI-")).join(pd.Series(ADX, name="ADX"))
    return df

def ADX_glm(df, n):
    pos_M = df["High"] - df["High"].shift(1)
    neg_M = df["Low"].shift(1) - df["Low"]
    DX_df = pd.DataFrame({"pos_M": pos_M,
                          "neg_M": neg_M,
                          "DM_up": pos_M[(pos_M > neg_M) & (pos_M > 0)],
                          "DM_down": neg_M[(neg_M > pos_M) & (neg_M > 0)]})
    DX_df[["DM_up", "DM_down"]] = DX_df[["DM_up", "DM_down"]].fillna(0).ewm(alpha=1 / n).mean()
    ADX = pd.Series(pd.Series.ewm(abs(DX_df['DM_up'] - DX_df['DM_down']) \
                                  / (DX_df['DM_up'] + DX_df['DM_down']), alpha=1 / n, min_periods=n).mean(), name = 'ADX')
    return ADX

# MACD, MACD Signal and MACD difference
def MACD(df, n_fast, n_slow, n_sig):
    EMAfast = pd.Series(df['Close'].ewm(span=n_fast, min_periods=n_slow - 1).mean())
    EMAslow = pd.Series(df['Close'].ewm(span=n_slow, min_periods=n_slow - 1).mean())
    MACD = pd.Series(EMAfast - EMAslow, name='MACD_' + str(n_fast) + '_' + str(n_slow))
    MACDsign = pd.Series(MACD.ewm(span=n_sig, min_periods=n_sig - 1).mean(),
                         name='MACDsign_' + str(n_fast) + '_' + str(n_slow))
    MACDdiff = pd.Series(MACD - MACDsign, name='MACDdiff_' + str(n_fast) + '_' + str(n_slow))
    df = df.join(MACD)
    df = df.join(MACDsign)
    df = df.join(MACDdiff)
    return df

def MACD_glm(df, n_fast, n_slow, n_sig, ratio = True):
    EMAfast = pd.Series(df['Close'].ewm(span=n_fast, min_periods=n_slow - 1).mean())
    EMAslow = pd.Series(df['Close'].ewm(span=n_slow, min_periods=n_slow - 1).mean())
    MACD = pd.Series(EMAfast - EMAslow)
    MACDsign = pd.Series(MACD.ewm(span=n_sig, min_periods=n_sig - 1).mean())
    MACDdiff = pd.Series((MACD - MACDsign), name = 'MACD_diff')
    if ratio:
        ratio = pd.Series(MACDdiff/MACDsign, name = 'MACD%')
        return ratio
    else:
        return MACDdiff

# Mass Index
def MassI(df):
    Range = df['High'] - df['Low']
    EX1 = pd.ewm(Range, span=9, min_periods=9).mean()
    EX2 = pd.ewm(EX1, span=9, min_periods=9).mean()
    Mass = EX1 / EX2
    MassI = pd.Series(pd.rolling_sum(Mass, 25), name='Mass Index')
    df = df.join(MassI)
    return df


# Vortex Indicator: http://www.vortexindicator.com/VFX_VORTEX.PDF
def Vortex(df, n):
    vm_pos = abs(df["High"] - df["Low"].shift(1)).rolling(window=n).sum()
    vm_neg = abs(df["Low"] - df["High"].shift(1)).rolling(window=n).sum()
    HL = df["High"] - df["Low"]
    HC = abs(df["High"] - df["Close"].shift(1))
    LC = abs(df["Low"] - df["Close"].shift(1))
    tr = pd.concat([HL, HC, LC], axis=1).max(axis=1).rolling(window=n).sum()
    VI = pd.DataFrame({'+VI' + str(n): vm_pos / tr, '-VI' + str(n): vm_neg / tr})
    ratio = pd.Series(VI['+VI' + str(n)]/VI['-VI' + str(n)], name = 'Vor%')
    df = df.join(VI)
    df = df.join(ratio)
    return df

def Vortex_glm(df, n, ratio = True):
    vm_pos = abs(df["High"] - df["Low"].shift(1)).rolling(window=n).sum()
    vm_neg = abs(df["Low"] - df["High"].shift(1)).rolling(window=n).sum()
    if ratio:
        ratio = pd.Series(vm_pos/vm_neg-1, name = 'Vor%')
        return ratio
    else:
        HL = df["High"] - df["Low"]
        HC = abs(df["High"] - df["Close"].shift(1))
        LC = abs(df["Low"] - df["Close"].shift(1))
        tr = pd.concat([HL, HC, LC], axis=1).max(axis=1).rolling(window=n).sum()
        diff = pd.Series((vm_pos - vm_neg) / tr, name = 'Vor_diff')
        return diff
 
  
# KST Oscillator
def KST(df, r1, r2, r3, r4, n1, n2, n3, n4):
    M = df['Close'].diff(r1 - 1)
    N = df['Close'].shift(r1 - 1)
    ROC1 = M / N
    M = df['Close'].diff(r2 - 1)
    N = df['Close'].shift(r2 - 1)
    ROC2 = M / N
    M = df['Close'].diff(r3 - 1)
    N = df['Close'].shift(r3 - 1)
    ROC3 = M / N
    M = df['Close'].diff(r4 - 1)
    N = df['Close'].shift(r4 - 1)
    ROC4 = M / N
    KST = pd.Series(ROC1.rolling(window=n1).sum() + ROC2.rolling(window=n2).sum() * 2 + ROC3.rolling(
        window=n3).sum() * 3 + ROC4.rolling(window=n4).sum() * 4,
                    name='KST_' + str(r1) + '_' + str(r2) + '_' + str(r3) + '_' + str(r4) + '_' + str(n1) + '_' + str(
                        n2) + '_' + str(n3) + '_' + str(n4))
    df = df.join(KST)
    return df


# Relative Strength Index
def RSI(df, n):
    gain = df["Close"].diff(1)
    gain[gain < 0] = 0
    loss = -df["Close"].diff(1)
    loss[loss < 0] = 0
    RSI = pd.Series(
        1 - 1 / (1 + gain.ewm(alpha=1 / n, min_periods=n).mean() / loss.ewm(alpha=1 / n, min_periods=n).mean()),
        name='RSI_' + str(n))
    df = df.join(RSI)
    return df

def RSI_glm(df, n):
    gain = df["Close"].diff(1)
    gain[gain < 0] = 0
    loss = -df["Close"].diff(1)
    loss[loss < 0] = 0
    RSI = pd.Series(1 - 1 / (1 + gain.ewm(alpha=1 / n, min_periods=n).mean() / loss.ewm(alpha=1 / n, min_periods=n).mean()), name = 'RSI')
    return RSI


# True Strength Index
def TSI(df, r, s):
    M = pd.Series(df['Close'].diff(1))
    aM = abs(M)
    EMA1 = M.ewm(span=r, min_periods=r).mean()
    aEMA1 = aM.ewm(span=r, min_periods=r).mean()
    EMA2 = EMA1.ewm(span=s, min_periods=s).mean()
    aEMA2 = aEMA1.ewm(span=s, min_periods=s).mean()
    TSI = pd.Series(EMA2 / aEMA2, name='TSI_' + str(r) + '_' + str(s))
    df = df.join(TSI)
    return df


# Accumulation/Distribution
def ACCDIST(df, n):
    ad = (2 * df['Close'] - df['High'] - df['Low']) / (df['High'] - df['Low']) * df['Volume']
    M = ad.diff(n - 1)
    N = ad.shift(n - 1)
    ROC = M / N
    AD = pd.Series(ROC, name='Acc/Dist_ROC_' + str(n))
    df = df.join(AD)
    return df


# Chaikin Oscillator
def Chaikin(df):
    ad = (2 * df['Close'] - df['High'] - df['Low']) / (df['High'] - df['Low']) * df['Volume']
    Chaikin = pd.Series(ad.ewm(span=3, min_periods=2).mean() - ad.ewm(span=10, min_periods=9).mean(), name='Chaikin')
    df = df.join(Chaikin)
    return df


# Money Flow Index and Ratio: volume-weighted RSI
def MFI(df, n):
    PP = (df['High'] + df['Low'] + df['Close']) / 3
    #    i = 0
    #    PosMF = [0]
    #    while i < df.index[-1]:
    #        if PP[i + 1] > PP[i]:
    #            PosMF.append(PP[i + 1] * df.get_value(i + 1, 'Volume'))
    #        else:
    #            PosMF.append(0)
    #        i = i + 1
    ud_i = ((PP > PP.shift(1)) + 0).replace(to_replace=0, value=-1)
    TotMF = PP * df['Volume']
    PosMF = pd.Series(TotMF * ud_i).clip(lower=0)
    MFI = pd.Series(PosMF.rolling(window=n).sum() / TotMF.rolling(window=n).sum(), name='MFI_' + str(n))
    df = df.join(MFI)
    return df


# On-balance Volume
def OBV(df):
    u_i = pd.Series(df.loc[df["Close"] > df["Close"].shift(1), "Close"])
    u_i[:] = 1
    d_i = pd.Series(df.loc[df["Close"] < df["Close"].shift(1), "Close"])
    d_i[:] = -1
    e_i = pd.Series(df.loc[df["Close"] == df["Close"].shift(1), "Close"])
    e_i[:] = 0
    ude_i = pd.concat([u_i, d_i, e_i])
    OBV_change = pd.Series((df["Volume"] * ude_i), name="OBV change")
    OBV = pd.Series(OBV_change.cumsum(), name="OBV")
    df = df.join(OBV_change).join(OBV)
    return df


# Force Index
def FORCE(df, n):
    F = pd.Series(df['Close'].diff(n) * df['Volume'].diff(n), name='Force_' + str(n))
    df = df.join(F)
    return df


# Ease of Movement
def EOM(df, n):
    EoM = (df['High'] + df['Low'] - df['High'].diff(1) - df['Low'].diff(1)) * (df['High'] - df['Low']) / (
                2 * df['Volume'])
    Eom_ma = pd.Series(pd.rolling_mean(EoM, n), name='EoM_' + str(n))
    df = df.join(Eom_ma)
    return df


# Commodity Channel Index
def CCI(df, n):
    PP = (df['High'] + df['Low'] + df['Close']) / 3
    CCI = pd.Series((PP - PP.rolling(window=n).mean()) / PP.rolling(window=n).std(), name='CCI_' + str(n))
    df = df.join(CCI)
    return df

def CCI_glm(df,n):
    PP = (df['High'] + df['Low'] + df['Close']) / 3
    CCI = pd.Series((PP - PP.rolling(window=n).mean()) / PP.rolling(window=n).std(), name = 'CCI')
    return CCI


# Coppock Curve
def COPP(df, n):
    M = df['Close'].diff(int(n * 11 / 10) - 1)
    N = df['Close'].shift(int(n * 11 / 10) - 1)
    ROC1 = M / N
    M = df['Close'].diff(int(n * 14 / 10) - 1)
    N = df['Close'].shift(int(n * 14 / 10) - 1)
    ROC2 = M / N
    Copp = pd.Series((ROC1 + ROC2).ewm(span=n, min_periods=n).mean(), name='Copp_' + str(n))
    df = df.join(Copp)
    return df


# Keltner Channel
def KELCH(df, n):
    KelChM = pd.Series(pd.rolling_mean((df['High'] + df['Low'] + df['Close']) / 3, n), name='KelChM_' + str(n))
    KelChU = pd.Series(pd.rolling_mean((4 * df['High'] - 2 * df['Low'] + df['Close']) / 3, n), name='KelChU_' + str(n))
    KelChD = pd.Series(pd.rolling_mean((-2 * df['High'] + 4 * df['Low'] + df['Close']) / 3, n), name='KelChD_' + str(n))
    df = df.join(KelChM)
    df = df.join(KelChU)
    df = df.join(KelChD)
    return df


# Ultimate Oscillator, average n period and weighted by m
def ULTOSC(df, n1=7, n2=14, n3=28, m1=4, m2=2, m3=1):
    BP = df["Close"] - pd.DataFrame({"pri Close": df["Close"].shift(1), "Low": df["Low"]}).min(axis=1)
    TR = pd.DataFrame({"pri Close": df["Close"].shift(1), "High": df["High"]}).max(axis=1) - \
         pd.DataFrame({"pri Close": df["Close"].shift(1), "Low": df["Low"]}).min(axis=1)
    UltO = pd.Series((m1 * BP.rolling(window=n1).sum() / TR.rolling(window=n1).sum() \
                      + m2 * BP.rolling(window=n2).sum() / TR.rolling(window=n2).sum() \
                      + m3 * BP.rolling(window=n3).sum() / TR.rolling(window=n3).sum()) / (m1 + m2 + m3)
                     , name='Ultimate_Osc')
    df = df.join(UltO)
    return df


# Donchian Channel
def DONCH(df, n):
    #    i = 0
    #    DC_l = []
    #    while i < n - 1:
    #        DC_l.append(0)
    #        i = i + 1
    #    i = 0
    #    while i + n - 1 < df.index[-1]:
    #        DC = max(df['High'].ix[i:i + n - 1]) - min(df['Low'].ix[i:i + n - 1])
    #        DC_l.append(DC)
    #        i = i + 1
    up = pd.Series(df["High"].rolling(window=n).max(), name="Donch upper")
    do = pd.Series(df["Low"].rolling(window=n).min(), name="Donch lower")
    mid = pd.Series((up + do) / 2, name="Donch central")
    dist = pd.Series(up - do, name="Donch distance")
    df = df.join(up).join(do).join(mid).join(dist)
    return df


# Standard Deviation
def STDDEV(df, n):
    df = df.join(pd.Series(df['Close'].rolling(window=n).std(), name='STD_' + str(n)))
    return df


# William Percent (update)
def WLP(df, n):
    df = df.join(pd.Series(-(df["High"].rolling(window=n).max() - df["Close"]) / \
                           (df["High"].rolling(window=n).max() - df["Low"].rolling(window=n).min()), \
                           name='W%_' + str(n)))
    return df

def WLP_glm(df, n):
    HH =  df["High"].rolling(window=n).max()
    WLP = pd.Series(-(HH - df["Close"]) / (HH - df["Low"].rolling(window=n).min()), name = 'w%')
    return WLP

# LP (update)
def LP(df):
    temp_df = pd.DataFrame({'ENV55': df["Close"] / df["Close"].ewm(span=55, min_periods=55).mean() - 1})
    temp_df.loc[(temp_df["ENV55"] < 0.01) & (temp_df["ENV55"] > 0), "LP"] = "Long"
    temp_df.loc[(temp_df["ENV55"] < 0) & (temp_df["ENV55"] > -0.01), "LP"] = "Short"
    temp_df.loc[abs(temp_df["ENV55"]) > 0.03, "LP"] = "Extreme"
    df = df.join(temp_df)
    return df


# HB (update), tfm = timeframe_multiplier, n = max_periods (shortest timeframe)
def HB(df, n, tfm=None, pos_window = 250):
    df.dropna(inplace=True)
    TR_s_n = pd.Series(TR_sp(df, n), name = "TR_" + str(n))
    ATR_n = pd.Series(ATR_sp(df, n), name = "ATR_" + str(n))
    HB = pd.Series(TR_s_n / (n * ATR_n), name="HB")
    HB_pos = pd.Series(HB.rolling(window= pos_window).apply(lambda x: pd.Series(x).rank(pct=True).iloc[-1]), name="HB_pos")
    return df.join(HB).join(HB_pos).join(TR_s_n).join(n * ATR_n)


def HB_glm(df, n):
    df.dropna(inplace=True)
    TR_s_n = pd.Series(TR_sp(df, n), name = "TR_" + str(n))
    ATR_n = pd.Series(ATR_sp(df, n), name = "ATR_" + str(n))
    HB = pd.Series(TR_s_n / (n * ATR_n), name="HB")
    return HB


# TH (update)
def TH(df):
    temp_df = pd.DataFrame({"US": (df[["Open", "Close"]].max(axis=1) - df["High"]) / (df["High"] - df["Low"]),
                            "LS": (df[["Open", "Close"]].min(axis=1) - df["Low"]) / (df["High"] - df["Low"])})
    temp_df.loc[temp_df["LS"] > 0.5, "t/h"] = "HANG"
    temp_df.loc[temp_df["US"] < -0.5, "t/h"] = "TOWER"
    df = df.join(temp_df)
    return df


def TH_sp(df):
    temp_df = pd.DataFrame({"US": (df[["Open", "Close"]].max(axis=1) - df["High"]) / (df["High"] - df["Low"]),
                            "LS": (df[["Open", "Close"]].min(axis=1) - df["Low"]) / (df["High"] - df["Low"])})
    temp_df.loc[temp_df["LS"] > 0.5, "t/h"] = "HANG"
    temp_df.loc[temp_df["US"] < -0.5, "t/h"] = "TOWER"
    return temp_df["t/h"]
    

def TH_glm(df):
    us = df["High"] - df[["Open", "Close"]].max(axis=1)
    ls = df[["Open", "Close"]].min(axis=1) - df["Low"]
    TH = pd.Series((us-ls)/(df["High"] - df["Low"]), name = 'T/H')
    return TH


# 2K reverse (update)
def TKR(df):
    temp_df = pd.DataFrame({"c2": (df["Close"] - df["Low"].rolling(window=2).min()) / \
                                  (df['High'].rolling(window=2).max() - df['Low'].rolling(window=2).min())})
    temp_df.loc[(temp_df['c2'].shift(1) > 0.55) & (temp_df['c2'] < 0.35), '2k-Rev'] = "DD"
    temp_df.loc[(temp_df['c2'].shift(1) < 0.45) & (temp_df['c2'] > 0.65), '2k-Rev'] = 'DU'
    df = df.join(temp_df)
    return df


def TKR_sp(df):
    
    temp_df = pd.DataFrame({"c2": (df["Close"] - df["Low"].rolling(window=2).min()) / \
                                  (df['High'].rolling(window=2).max() - df['Low'].rolling(window=2).min())})
    temp_df.loc[(temp_df['c2'].shift(1) > 0.55) & (temp_df['c2'] < 0.35), '2k-Rev'] = "DD"
    temp_df.loc[(temp_df['c2'].shift(1) < 0.45) & (temp_df['c2'] > 0.65), '2k-Rev'] = 'DU'
    return temp_df["2k-Rev"]


def TKR_glm(df):
    c2 = pd.Series((df["Close"] - df["Low"].rolling(window=2).min()) / \
                                  (df['High'].rolling(window=2).max() - df['Low'].rolling(window=2).min()), name = 'TKR')
    return c2


# 3D trend (update)
def TDT_t(df):
    temp_df = pd.DataFrame({'c3': (df["Close"] - df["Low"].rolling(window=3).min()) / \
                                  (df['High'].rolling(window=3).max() - df['Low'].rolling(window=3).min())})
    temp_df.loc[temp_df['c3'] > 0.6, '3d-trend'] = 'Buy'
    temp_df.loc[temp_df['c3'] < 0.4, '3d-trend'] = 'Sell'
    temp_df.iloc[1, temp_df.columns.get_loc('3d-trend')] = 'Sell' if df['Close'].iloc[0] > df['Close'].iloc[1] else 'Buy'
           
    for i in temp_df[temp_df['3d-trend'].isnull()].index[1:]:
        if ((temp_df.loc[i-1, '3d-trend'] == 'Sell') & (temp_df.loc[i, 'c3'] > 0.6)) | ((temp_df.loc[i-1, '3d-trend'] == 'Buy') & (temp_df.loc[i, 'c3'] >= 0.4)):
            temp_df.loc[i, '3d-trend'] = 'Buy'
        else:
            temp_df.loc[i, '3d-trend'] = 'Sell'
    df = df.join(temp_df)
    return df

def TDT_glm(df):
    c3 = pd.Series((df["Close"] - df["Low"].rolling(window=3).min()) / \
                   (df['High'].rolling(window=3).max() - df['Low'].rolling(window=3).min()), name = 'TDT')
    return c3


# 9 turn (update)
def NT(df, perf_n=1):
    temp_df = pd.DataFrame({"diff": df["Close"].diff(4) < 0})
    temp_df.loc[:, 'counter_buy'] = cum_counter(temp_df["diff"])
    temp_df.loc[:, 'counter_sell'] = cum_counter(1 - temp_df["diff"])
    temp_df.loc[(temp_df['counter_buy'].shift(2) == 7) & (
                (df['Low'].shift(1).diff(1) < 0) | (df['Low'].shift(1).diff(2) < 0)), 'Signal'] = 'Buy'
    temp_df.loc[(temp_df['counter_buy'].shift(2) == 7) & (
                (df['Low'].diff(2) < 0) | (df['Low'].diff(3) < 0)), 'Signal'] = 'Buttom'
    temp_df.loc[(temp_df['counter_sell'].shift(2) == 7) & (
                (df['High'].shift(1).diff(1) > 0) | (df['High'].shift(1).diff(2) > 0)), 'Signal'] = 'Sell'
    temp_df.loc[(temp_df['counter_sell'].shift(2) == 7) & (
                (df['High'].diff(2) > 0) | (df['High'].diff(3) > 0)), 'Signal'] = 'Top'
    df = df.join(temp_df['Signal'])
    return df


# Immonotonic fractal
def IMFRAC(df, n):
    win_max = pd.Series.rolling(df['High'], n, center=True).max()
    win_min = pd.Series.rolling(df['Low'], n, center=True).min()

    df['up_frac'] = np.where(df['High'] == win_max, df['High'], np.nan)
    df['down_frac'] = np.where(df['Low'] == win_min, df['Low'], np.nan)

    return df


# Monotonic fractal
def MOFRAC(df, n):
    win_max = pd.Series.rolling(df['High'], n, center=True).max()
    win_min = pd.Series.rolling(df['Low'], n, center=True).min()

    df['up_frac'] = np.where((df['High'] == win_max) \
                             & (df["High"].shift(2) < df["High"].shift(1)) \
                             & (df["High"].shift(-2) < df["High"].shift(-1)) \
                             , df['High'], np.nan)
    df['down_frac'] = np.where((df['Low'] == win_min) \
                               & (df["Low"].shift(2) > df["Low"].shift(1)) \
                               & (df["Low"].shift(-2) > df["Low"].shift(-1)) \
                               , df['Low'], np.nan)
    return df


# 3 Day Reverse
def TDR(df):
    ut = df.loc[(df['High'] > df['High'].shift(1)) & (df['High'].shift(1) > df['High'].shift(2)), 'Low']
    dt = df.loc[(df['Low'] < df['Low'].shift(1)) & (df['Low'].shift(1) < df['Low'].shift(2)), 'High']
    
    ut.loc[(ut < ut.shift(1)) & (ut.shift(1) > ut.shift(2))] = 0
    dt.loc[(dt > dt.shift(1)) & (dt.shift(1) < dt.shift(2))] = 0
    
    utv = pd.Series(ut, name = 'UTL')
    dtv = pd.Series(dt, name = 'DTH')
    
    temp_df = df.join(utv).join(dtv)
    
    ut = list(temp_df["UTL"])
    dt = list(temp_df["DTH"])
    high = list(df["High"])
    low = list(df["Low"])
    
    temp_df.drop(["UTL", "DTH"], axis = 1, inplace = True)
        
    tdrev = {}
    current_utl = 0
    current_dth = 0
    
    for i in range(len(low)):
        # initialize values
        if current_utl == 0 and current_dth == 0:
            if pd.notnull(ut[i]):
                current_utl = ut[i]
            elif pd.notnull(dt[i]):
                current_dth = dt[i]
        # when utl is valid and dth = 0
        elif current_utl != 0:
            # check if break by low
            if low[i] < current_utl:
                current_utl = 0
                current_dth = high[i]
            # check if should update
            elif ut[i] > current_utl:
                current_utl = ut[i]

        # when dth is valid and utl = 0
        elif current_dth != 0:
            # check if break by high
            if high[i] > current_dth:
                current_dth = 0
                current_utl = low[i]
            # check if should update
            elif dt[i] < current_dth:
                current_dth = dt[i]

        tdrev[i] = {"UTL": current_utl, 
                     "DTH": current_dth}
        
    tech_df = pd.DataFrame(tdrev).transpose().set_index(temp_df.index)
    
    temp_df["UTL"] = tech_df["UTL"].replace(0, np.nan)
    temp_df["DTH"] = tech_df["DTH"].replace(0, np.nan)
#    df = df.fillna(method = 'ffill')
    return temp_df

def TDR_glm(df, ratio = True):
    df = TDR(df)
    if ratio:
        df.loc[pd.notnull(df['DTH']), 'TDR%'] = (df['High'] - df['DTH'])/df['High']
        df.loc[pd.notnull(df['UTL']), 'TDR%'] = (df['Low'] - df['UTL'])/df['Low']
    else:
        df.loc[pd.notnull(df['DTH']), 'TDR%'] = df['High'] - df['DTH']
        df.loc[pd.notnull(df['UTL']), 'TDR%'] = df['Low'] - df['UTL']
    return df['TDR%']


#Envelope
def ENV(df, n, k=0.005, EMA_method=False):
    if EMA_method == True:
        MA = df['Close'].ewm(span=n, min_periods=n).mean()
    else:
        MA = df['Close'].rolling(window=n).mean()
        
    env = pd.Series((df['Close'] - (1 - k) * MA) / (2 * k * MA), name="env%")
    df = df.join(env)
    return df

def ENV_glm(df, n, k=0.005, EMA_method=False):
    if EMA_method:
        MA = df['Close'].ewm(span=n, min_periods=n).mean()
    else:
        MA = df['Close'].rolling(window=n).mean()
        ENV = pd.Series((df['Close'] - (1 - k) * MA) / (2 * k * MA), name = 'ENV%')
    return ENV
    

#Alligator
def ALLI(df, n=[13,8,5], shift=[8,5,3], price='Median', method='SMMA'):
#price={'Open','High','Low','Close','Median','Typical'}
#method={'SMA','EMA','SMMA'}
    if price == 'Median':
        data = (df['High']+df['Low'])/2
    elif price == 'Typical':
        data = (df['High']+df['Low']+df['Close'])/3
    else:
        data = df[price]
    
    alli = pd.DataFrame(columns=['Jaw','Teeth','Lips'])    
    if method == 'SMA':
        for i in range(len(n)):
            alli.iloc[:,i] = data.rolling(window=n[i]).mean().shift(shift[i])
    elif method == 'EMA':
        for i in range(len(n)):
            alli.iloc[:,i] = data.ewm(span=n[i], min_periods=n[i]).mean().shift(shift[i])
    elif method == 'SMMA':
        n_s = pd.Series(n)*2-1
        for i in range(len(n_s)):
            alli.iloc[:,i] = data.ewm(span=n_s[i], min_periods=n_s[i]).mean().shift(shift[i])
            
    A1 = pd.Series(alli['Jaw']/alli['Teeth']-1, name='Alli%d1')
    A2 = pd.Series(alli['Teeth']/alli['Lips']-1, name='Alli%d2')
    df = df.join(A1).join(A2)
    return df

#special Alligator only return dev
def ALLI_sp(df, n=[13,8,5], shift=[8,5,3], price='Median', method='SMMA'):
#price={'Open','High','Low','Close','Median','Typical'}
#method={'SMA','EMA','SMMA'}
    if price == 'Median':
        data = (df['High']+df['Low'])/2
    elif price == 'Typical':
        data = (df['High']+df['Low']+df['Close'])/3
    else:
        data = df[price]
    
    alli = pd.DataFrame(columns=['Jaw','Teeth','Lips'])    
    if method == 'SMA':
        for i in range(len(n)):
            alli.iloc[:,i] = data.rolling(window=n[i]).mean().shift(shift[i])
    elif method == 'EMA':
        for i in range(len(n)):
            alli.iloc[:,i] = data.ewm(span=n[i], min_periods=n[i]).mean().shift(shift[i])
    elif method == 'SMMA':
        n_s = pd.Series(n)*2-1
        for i in range(len(n_s)):
            alli.iloc[:,i] = data.ewm(span=n_s[i], min_periods=n_s[i]).mean().shift(shift[i])
            
    A1 = pd.Series(alli['Jaw']/alli['Teeth']-1, name='Alli%d1')
    A2 = pd.Series(alli['Teeth']/alli['Lips']-1, name='Alli%d2')
    df = df.join(A1).join(A2)
    return pd.concat([A1, A2], axis = 1)


# AC
def AC(df, fast_n, slow_n, div_n):
    price_median = (df["High"] + df["Low"])/2
    ao = pd.Series.rolling(price_median, fast_n).mean() - pd.Series.rolling(price_median, slow_n).mean()
    ac = ao - pd.Series.rolling(ao, div_n).mean()
    temp_df = df.join(pd.Series(ac, name = "AC"))
    temp_df.loc[temp_df["AC"].diff(1) > 0, "color"] = "green"
    temp_df.loc[temp_df["AC"].diff(1) <= 0, "color"] = "red"
    return temp_df

def AC_glm(df, fast_n, slow_n, div_n, ratio = True):
    price_median = (df["High"] + df["Low"])/2
    ao = pd.Series.rolling(price_median, fast_n).mean() - pd.Series.rolling(price_median, slow_n).mean()
    ac = ao - pd.Series.rolling(ao, div_n).mean()
    diff = pd.Series(ac.diff(1), name = 'AC_diff')
    if ratio:
        ratio = pd.Series(diff/ac.shift(1), name = 'AC%')
        return ratio
    else:
        return diff


# give two std: (X - x) > 0 only & (X - x) < 0 only
def one_side_std(logR, isPos = True):
    logR_len = pd.notnull(logR).sum()
    if isPos:
        std0 = sum((logR[logR > 0] ** 2)/(logR_len - 1)) ** (1/2)
    else:
        std0 = sum((logR[logR < 0] ** 2)/(logR_len - 1)) ** (1/2)
    return std0


# import price for tech indicator calculate
def import_price_ti(data_path, pre_period, start_date, end_date = None, str_datetime = False):
    price_df = pd.read_csv(data_path, header = None)
    price_df.columns = ["Date","Time","Open","High","Low","Close","Volume"]
    price_df["Datetime"] = price_df["Date"] + " " + price_df["Time"]
    dt = pd.to_datetime(price_df["Datetime"])
    if not str_datetime:
        price_df["Datetime"] = dt
        
    data_start_index = price_df[dt >= pd.to_datetime(start_date)].index[0] - pre_period + 1
    price_df = price_df[data_start_index:]

    if end_date is not None:
        price_df = price_df[dt <= pd.to_datetime(end_date)]
    if "JPY" in data_path:
        p = 100
    elif "XAU" in data_path:
        p = 10
    elif "VIX" in data_path:
        p = 100
    elif "DJ" in data_path:
        p = 1
    elif "LTC" in data_path:
        p = 10
    else:
        p = 10000
    price_df["pips"] = p
    return price_df


# RAVI
def RAVI(df, fast_n, slow_n):
    MA_fast = MA_sp(df, fast_n)
    MA_slow = MA_sp(df, slow_n)
    ravi = pd.Series((MA_fast - MA_slow)/MA_slow * 100, name = 'RAVI')
    return df.join(ravi)

def RAVI_glm(df, fast_n, slow_n, ratio = True):
    MA_fast = MA_sp(df, fast_n)
    MA_slow = MA_sp(df, slow_n)
    diff = pd.Series(MA_fast - MA_slow, name = 'RAVI_diff')
    if ratio:
        ratio = pd.Series(diff/MA_slow, name = 'RAVI%')
        return ratio
    else:
        return diff


# Parabolic SAR
def PSAR_mt4(df, step = 0.02, max_ratio = 0.2):
    high = df["High"].iloc[4:]
    low = df["Low"].iloc[4:]
    hh = df["High"].iloc[:4].max()
    ll = df["Low"].iloc[:4].min()
    isUp = True
    sar = ll
    ratio = step
    sar_dict = {"up": {}, "down": {}}
    for i in high.index[1:]:
        # check direction
        if isUp:
            # update sar
            sar = sar + ratio * (hh - sar)
            # check if update hh                
            if high.loc[i] > hh:
                hh = high.loc[i]
                if ratio < max_ratio:
                    ratio += step
                
        else:
            # update sar
            sar = sar - ratio * (sar - ll)
            # check if update ll                
            if low.loc[i] < ll:
                ll = low.loc[i]
                if ratio < max_ratio:
                    ratio += step          
        # decide if trend reverse (always assume uptrend for the initialize)
        # for downtrend, if current high > sar, trend reverse
        # for uptrend, if current Low < sar, trend reverse
        if (isUp and low.loc[i] < sar) or (not isUp and high.loc[i] > sar):
            ratio = step
            if isUp:
                isUp = False
                ll = low.loc[i]
                sar = hh
                sar_dict["down"][i] = sar
            else:
                isUp = True
                hh = high.loc[i]
                sar = ll
                sar_dict["up"][i] = sar                
        # the current trend continue
        else:
            if isUp:
                sar_dict["up"][i] = sar
            else:
                sar_dict["down"][i] = sar                
            # check if update ratio
    return df.join(pd.DataFrame(sar_dict))

# Parabolic SAR ratio
def PSAR_ratio(df, step = 0.02, max_ratio = 0.2, adjust_init = False):
    df = PSAR_mt4(df, step, max_ratio)
    df.loc[pd.notnull(df['down']), 'PSAR%'] = (df['High']-df['down'])/df['High']
    df.loc[pd.notnull(df['up']), 'PSAR%'] = (df['Low']-df['up'])/df['Low']
    df = df.dropna(subset=['PSAR%'])
    
    if adjust_init:
        num = 0
        for i in df.index[1:]:
            if df['PSAR%'][i] * df['PSAR%'][i-1]<0:
                num += 1
                if num == 2:
                    i_start = i
                    break
        df = df.loc[i_start:]
    return df

def PSAR_glm(df, step = 0.02, max_ratio = 0.2):
    df = PSAR_mt4(df, step, max_ratio)
    df.loc[pd.notnull(df['down']), 'PSAR%'] = (df['High']-df['down'])/df['High']
    df.loc[pd.notnull(df['up']), 'PSAR%'] = (df['Low']-df['up'])/df['Low']
    return df['PSAR%']


# Heikin-Ashi
def HA(df):
    HAC = pd.Series((df['Open'] + df['High'] + df['Low'] + df['Close']) / 4, name = 'HA_C')
    HAO = pd.Series(index = HAC.index, name = 'HA_O')
    HAO.iloc[0] = df['Open'].values[0]   
    for i in HAC.index[1:]:
        HAO[i] = (HAO[i-1] + HAC[i-1]) / 2 
    df = df.join(HAO).join(HAC)
    
    df['HA_H'] = df[['High', 'HA_O', 'HA_C']].max(axis=1)
    df['HA_L'] = df[['Low', 'HA_O', 'HA_C']].min(axis=1)
    
    df.loc[df['HA_C'] < df['HA_O'], 'trend'] = 'down'
    df.loc[df['HA_C'] > df['HA_O'], 'trend'] = 'up'
    return df

def HA_glm(df):
    HAC = (df['Open'] + df['High'] + df['Low'] + df['Close']) / 4
    HAO = pd.Series(np.nan, index = HAC.index)
    HAO.iloc[0] = df.iloc[0, 2]
    for i in HAC.index[1:]:
        HAO[i] = (HAO[i-1] + HAC[i-1]) / 2
    HA_ratio = pd.Series(HAC/HAO-1, name = 'HA%')
    return HA_ratio


# =============================================================================
# =================================== TEST ====================================

# from Helper import data_import as di
# from Helper import file_location_settings as fls

# df = di.import_price_mt4csv(fls.data_loc + "XAUUSD60.csv")






















