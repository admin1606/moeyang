# -*- coding: utf-8 -*-
"""
Created on Thu Nov 15 11:01:39 2018

@author: Moe Yang
"""

# =============================================================================
# Basic version of Grid
# Open order by bar
# Untriggerred order will be cancelled at next bar
# Auto-adjusted step & TP level
# =============================================================================


from Optimize_pars.Strategies import trade_class as tc, techIndicators as ti


def main(price_chart, lots, top_lv, bot_lv, bar_time_frame, atr_time_frame, 
         atr_period, atr_step_ratio, atr_tp_ratio):
    
    atr = ti.ATR_sp(price_chart.chart_dict[atr_time_frame].set_index("Datetime"), atr_period).shift().dropna()
    open_index = price_chart.chart_dict[bar_time_frame].set_index("Datetime").index
    trade_book = tc.Trade_book(price_chart.name)
    bar_index0 = None
    
    for index, tick in price_chart.chart_dict[0].iterrows():

        # update trade book
        trade_book.update(tick["Datetime"], tick["Ask"], tick["Bid"])

        # check if valid atr / next new bar
        bar_index1 = tick["Datetime"].floor(str(bar_time_frame) + "min")
        
        if bar_index1 != bar_index0 and bar_index1 in open_index:
            
            atr_index = tick["Datetime"].floor(str(atr_time_frame) + "min")
            
            if atr_index in atr.index:
                
                # update bar_index
                bar_index0 = bar_index1
                    
                # cancel untriggered pending orders
                trade_book.pend_orders.clear()
                        
                # update step            
                atr_step = atr_step_ratio * atr.loc[atr_index]
                tp_step = atr_tp_ratio * atr.loc[atr_index]
                
                for n in range(top_lv):
                    
                    open_price = tick["Ask"] + atr_step * (n + 1)
                    tp_price = open_price - tp_step
                    trade_book.send_pend_order(lots, "sell limit", open_price, tp_price = tp_price)
                    
                for m in range(bot_lv):
                    
                    open_price = tick["Bid"] - atr_step * (n + 1)
                    tp_price = open_price + tp_step
                    trade_book.send_pend_order(lots, "buy limit", open_price, tp_price = tp_price)
                    
        # record float pnl
        trade_book.float_record()
                
    trade_book.close_all("close by stop")
                                
    return trade_book

#vix = tc.Price_chart("VIX", 100)
#vix.fetch_all(True)
#tb = main(vix, 0.1, 2, 2, 60, 60, 6, 0.5, 1)
#
#new_pad = tc.Visual_pad("VIX Grid", 60)
#new_pad.draw_price(vix)
#new_pad.draw_trade_book(tb)
#new_pad.show_plot()
            
            
        
        
        