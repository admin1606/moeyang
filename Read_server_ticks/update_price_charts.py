# -*- coding: utf-8 -*-
"""
Created on Thu Dec  6 17:21:37 2018

@author: Moe Yang
"""

import pickle as pk

import pandas as pd
import psycopg2

from Optimize_pars.Strategies import trade_class as tc


def connect_db(databases):
    """
    Database connection
    """
    try:
        conn = psycopg2.connect(dbname=databases['DBNAME'], user=databases['DBUSER'],
                                password=databases['DBPASSWORD'], host=databases['DBHOST'],
                                port=databases['DBPORT'])
        print("connecting")

    except:
        print("Error on connecting to PostgreSQL")
        return

    return conn


def update_prices(database, price_pkl_path, log_path):
    """
    Read prices of all symbols available on server, then dump them
    as local pickle files.
    Note that it will directly overwrite old pickle price files.

    Args:
        database: a dict of price server connect information.
        price_pkl_path: a str of path to dump local pickle price.
        log_path: a str of path to dump price information log.

    Return:
        None
    """

    cur = connect_db(database).cursor()
    sql = "\n select symbol from bfj_price_ohlc_1mn group by symbol"
    cur.execute(sql)

    symb_lst = [v[0] for v in cur.fetchall()] # fetch all available symbols

    price_update_log = {}

    # Creates price chart for each symbol, fetch their prices and then store.
    # Time range / missing ticks information will be recorded.

    for symb in symb_lst:

        print("fetching " + symb + "'s prices...")
        price_chart = tc.Price_chart(symb, spread=30)
        price_chart.conn_serv() # create connection to server
        price_chart.fetch_bar_serv([1, 60, 240, 1440, 10080, 43200])

        # transfer all intra hour timeframe from 1min bar
        for tf in [5, 10, 15, 30]:
            price_chart.trans_bar(1, tf)

        price_chart.fetch_tick_serv()
        price_chart.cunn = None  # reset connection of server

        # record number of bar/ticks, start date & end date
        price_update_log[symb] = pd.DataFrame({tf: {"length": len(price_chart.chart_dict[tf]),
                                                    "start_date": min(price_chart.chart_dict[tf]["Datetime"]),
                                                    "end_date": max(price_chart.chart_dict[tf]["Datetime"])} \
                                               for tf in [0, 1, 60, 240, 1440, 10080, 43200]}).T[["length",
                                                                                                  "start_date",
                                                                                                  "end_date"]]

        # save price chart as pickle
        with open(price_pkl_path + "\\" + symb + ".pk", "wb") as f:
            pk.dump(price_chart, f)

    # save price update log details
    pd.concat(price_update_log).to_csv(log_path + "\\update_log.csv")
